require('app-module-path').addPath(__dirname)
const httpContext = require('express-http-context');

const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const express = require('express')
const accessControl = require('middleware/access-control.middleware')
const dotenv = require('dotenv').config()
const i18n = require("i18n")
const app = express()

app.use(i18n.init)

let i18nHelper = {}

i18n.configure({
  locales: ['en', 'es'],
  directory: __dirname + '/catalogs/locales',
  objectNotation: true,
  register: i18nHelper
});

global.__config = require('konphyg')('config')('main')
global.__logger = require('middleware/logger.middleware.js')
global.__i18n = i18nHelper
__logger.info(`NODE_ENV: ${process.env.NODE_ENV}`)



app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(accessControl)
app.use(httpContext.middleware);

require('routes')(app)

// Bootstrap crons
require('crons')()

module.exports = app
