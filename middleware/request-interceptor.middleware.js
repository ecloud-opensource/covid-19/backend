const { Op } = require('sequelize')
const sequelize = require('sequelize')
const _ = require('underscore')
const { getEndOfDayWithFormat, getStartOfDayWithFormat } = require('helpers/datetime.helper')
const {
	isArrayNullOrEmpty, isDateNullOrEmpty, isNull, isNullOrEmpty, isUndefined
} = require('helpers/validation.helper')


/**
 * @apiDefine QueryOptionsRequest
 * @apiParam (QueryOptions) {Integer} page Pagina
 * @apiParam (QueryOptions) {Integer} pageSize Tamaño de la página
 * @apiParam (QueryOptions) {String} code Código
 * @apiParam (QueryOptions) {String} name Nombre
 * @apiParam (QueryOptions) {String} email Email
 * @apiParam (QueryOptions) {String} description Descripción
 * @apiParam (QueryOptions) {String} from Fecha desde
 * @apiParam (QueryOptions) {String} to Fecha hasta
 * @apiParam (QueryOptions) {String} term Termino para buscar entre las columnas principales
 * @apiParam (QueryOptions) {String} scope Alcance query
 * @apiParam (QueryOptions) {String} group Por que termino deseas agrupar
 * @apiParam (QueryOptions) {String} order Por que termino deseas ordenar
 * @apiParam (QueryOptions) {String} status Por que estado deseas buscar
 * @apiParam (QueryOptions) {String="firstName,DESC"}} order Campo por el que se ordena la busqueda. (order=firstName,DESC)
 */

class QueryOptions {
	constructor(queryParams) {
		this.page
		this.pageSize

		this.from
		this.to
		this.fromTime
		this.toTime

		this.term = ''
		this.order = []
		this.group = ''
		this.extras = []

		this.scopes = ['defaultScope']

		this.query = {}

		for (const paramName in queryParams) {
			if (Object.prototype.hasOwnProperty.call(queryParams, paramName)) {
				switch (paramName) {
					case 'page': {
						this.page = parseInt(queryParams.page, 10)
						break
					}
					case 'pageSize': {
						this.pageSize = parseInt(queryParams.pageSize, 10)
						break
					}
					case 'from': {
						this.from = queryParams.from
						break
					}
					case 'to': {
						this.to = queryParams.to
						break
					}
					case 'fromTime': {
						this.fromTime = queryParams.fromTime
						break
					}
					case 'toTime': {
						this.toTime = queryParams.toTime
						break
					}
					case 'scope': {
						if (queryParams.scope.includes('removeDefault')) {
							this.scopes = []
							queryParams.scope = queryParams.scope.replace('removeDefault', '')
						}
						for (const scope of queryParams.scope.split(',')) {
							if (scope) {
								this.scopes.push(scope)
							}
						}
						break
					}
					case 'term': {
						this.term = queryParams.term
						break
					}
					case 'order': {
						this.order = queryParams.order.split(',')
						break
					}
					case 'group': {
						this.group = queryParams.group
						break
					}
					default: {
						this.extras.push({ [paramName]: (queryParams[paramName] === 'null' ? null : queryParams[paramName]) })
						break
					}
				}
			}
		}

		// Lazy build the query
		this.query = { where: {} }

		if (this.code) { this.query.where = _.extend({ code: this.code }, this.query.where) }
		if (this.status) { this.query.where = _.extend({ status: this.status }, this.query.where) }
		if (this.name) { this.query.where = _.extend({ name: { [Op.like]: `%${this.name}%` } }, this.query.where) }
		if (this.email) { this.query.where = _.extend({ email: { [Op.like]: `%${this.email}%` } }, this.query.where) }
		if (this.description) { this.query.where = _.extend({ description: { [Op.like]: `%${this.description}%` } }, this.query.where) }

		if (this.from || this.to) {
			const timeRangeArray = []
			if (this.from) { timeRangeArray.push({ [Op.gte]: getStartOfDayWithFormat(this.from, 'YYYY-MM-DD') }) }
			if (this.to) { timeRangeArray.push({ [Op.lte]: getEndOfDayWithFormat(this.to, 'YYYY-MM-DD') }) }
			this.query.where = _.extend({ createdAt: { [Op.and]: timeRangeArray } }, this.query.where)
		}
		if (!isUndefined(this.fromTime) || !isUndefined(this.toTime)) {
			if (!isDateNullOrEmpty(this.fromTime)) {
				this.query.where.fromTime = { [Op.gte]: getStartOfDayWithFormat(this.fromTime, 'YYYY-MM-DD') }
			} else if (isNull(this.fromTime)) {
				this.query.where.fromTime = null
			}
			if (!isDateNullOrEmpty(this.toTime)) {
				this.query.where.toTime = { [Op.lte]: getEndOfDayWithFormat(this.toTime, 'YYYY-MM-DD') }
			} else if (isNull(this.toTime)) {
				this.query.where.toTime = null
			}
		}
		if (this.pageSize) {
			if (!this.page) {
				this.page = 1
			}
			this.query.limit = this.pageSize
			this.query.offset = (this.page - 1) * this.query.limit
			if (this.scopes.length > 1) {
				this.query.separate = true
			}
		}

		for (const extra of this.extras) {
			this.query.where = _.extend(extra, this.query.where)
		}
		if (!isArrayNullOrEmpty(this.order)) {
			this.query.order = [this.order]
		}
		if (!isNullOrEmpty(this.group)) {
			this.query.group = [this.group]
		}

		//this.query.logging = console.log
	}

	getUnlimitedQuery() {
		return Object.assign(this.query, { limit: '', offset: '' })
	}

	addSearchTermColumns(columns) {
		if (!isNullOrEmpty(this.term)) {
			const orTerms = []
			for (const column of columns) {
				orTerms[`${column}`] = { [Op.iLike]: `%${this.term}%` }
			}
			this.query.where = {
				...this.query.where,
				[Op.or]: { ...orTerms }
			}
		}
	}

	addSearchTermChildColumns(columns, tableName, scope) {
		if (!isNullOrEmpty(this.term)
			&& this.scopes.indexOf(scope)) {
			const orTerms = []
			for (const column of columns) {
				const field = `$${tableName}.${column}$`
				orTerms[field] = { [Op.iLike]: `%${this.term}%` }
			}
			this.query.where = {
				...this.query.where,
				[Op.or]: { ...orTerms }
			}
		}
	}

	addCustomFieldChildColumns(columns, tableName, scope) {
		if (!isNullOrEmpty(this.term)
			&& (this.scopes.indexOf(scope) !== -1 || this.scopes.indexOf('tripAll') !== -1 || this.scopes.indexOf('all') !== -1)) {
			const orTerms = []
			for (const column of columns) {
				const field = `$${tableName}.${column}$`
				orTerms[field] = { $iLike: `%${this.term}%` }
			}
			this.query.where.$or = sequelize.or({ ...orTerms }, this.query.where.$or)
		}
	}

	addSearchTermConcatChildColumns(columns, tableName, scope) {
		if (!isNullOrEmpty(this.term)
			&& (this.scopes.indexOf(scope) !== -1 || this.scopes.indexOf('tripAll') !== -1 || this.scopes.indexOf('all') !== -1)) {
			const fields = []
			for (const column of columns) {
				fields.push(`'${tableName}'.'${column}'`)
			}
			this.query.where.$or = sequelize.or(
				sequelize.literal(`CONCAT(${fields.join(', \' \' ,')}) LIKE '%${this.term}%' `), this.query.where.$or
			)
		}
	}

	addSearchTermStatus(tableName) {
		if (!isNullOrEmpty(this.status)
			&& (this.scopes.indexOf('status') !== -1 || this.scopes.indexOf('tripAll') !== -1 || this.scopes.indexOf('all') !== -1)) {
			const orTerms = []
			const field = `$${tableName}.status.id$`
			orTerms[field] = { [Op.eq]: `${this.status}` }
			this.query.where.$and = sequelize.and({ $and: { ...orTerms } }, this.query.where.$and)
		}
	}

	addSearchTermStatusForStatus() {
		if (!isNullOrEmpty(this.status) && (this.scopes.indexOf('status') !== -1 || this.scopes.indexOf('extraData') !== -1)) {
			const orTerms = []
			const field = '$status.id$'
			orTerms[field] = { [Op.eq]: `${this.status}` }
			this.query.where.$and = sequelize.and({ $and: { ...orTerms } }, this.query.where.$and)
		}
	}

	getMetaList(totalResults, currentPageSize) {
		return {
			totalResults,
			currentPage: this.page ? this.page : 1,
			pageSize: currentPageSize
		}
	}

	addCustomOrder(order) {
		this.query.order = sequelize.literal(order)
	}

	addCustomGroupBy(groupBy) {
		this.query.group = sequelize.literal(groupBy)
	}

	addCustomAnd(query) {
		this.query.where.$and = sequelize.and(sequelize.literal(query), this.query.where.$and)
	}

	addCustomAndOnChildColumns(query, scope) {
		if (this.scopes.indexOf(scope) !== -1) {
			this.query.where.$and = sequelize.and(sequelize.literal(query), this.query.where.$and)
		}
	}

	addCustomOr(query) {
		this.query.where.$or = sequelize.or(sequelize.literal(query), this.query.where.$or)
	}

	addCustomBetween(field, from, to) {
		this.query.where[field] = {
			$between: [from, to]
		}
	}

	removeFieldFromWhere(field) {
		delete this.query.where[`${field}`]
	}

	addCustomAttribute(query, name) {
		this.query.attributes ?
			this.query.attributes.push([query, name]) :
			this.query.attributes = [[query, name]]
	}

}

exports.interceptor = (req, res, next) => {
	const options = {
		pageSize: 20,
		page: 1,
		scopes: ['defaultScope'],
		extras: [],
		query: {
			// logging: console.log
		}
	}

	const queryParams = req.query
	for (const paramName in queryParams) {
		if (Object.prototype.hasOwnProperty.call(queryParams, paramName)) {
			switch (paramName) {
				case 'page': {
					options.page = parseInt(queryParams.page, 10)
					break
				}
				case 'pageSize': {
					options.pageSize = parseInt(queryParams.pageSize, 10)
					break
				}
				case 'scopes': {
					if (queryParams.scopes.includes('removeDefault')) {
						options.scopes = []
						queryParams.scopes = queryParams.scopes.replace('removeDefault', '')
					}
					for (const scope of queryParams.scopes.split(',')) {
						if (scope) {
							options.scopes.push(scope)
						}
					}
					break
				}
				case 'order': {
					options.order = queryParams.order.split(',')
					break
				}
				case 'group': {
					options.group = queryParams.group
					break
				}
				default: {
					options.extras.push({ [paramName]: (queryParams[paramName] === 'null' ? null : queryParams[paramName]) })
					break
				}
			}
		}
	}

	if (options.pageSize) {
		if (!options.page) {
			options.page = 1
		}
		options.query.limit = options.pageSize
		options.query.offset = (options.page - 1) * options.query.limit
		if (options.scopes.length > 1) {
			options.query.separate = true
		}
	}

	req.options = options

	return next()
}

module.exports = (req, res, next) => {
	req.queryOptions = new QueryOptions(req.query)
	next()
}
