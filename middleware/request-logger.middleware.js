const _ = require('underscore')
const { v4 } = require('uuid')

exports.input = (req, res, next) => {
	res.txid = v4()
	let log = `${res.txid} >> ${req.method} - ${req.originalUrl}`
	if (req.body && !_.isEmpty(req.body)) { log = log.concat(` >> ${JSON.stringify(req.body)}`) }
	__logger.info(log)
	next()
}

exports.output = (res, body = {}) => {
	__logger.info(`${res.txid} << ${res.statusCode} - ${JSON.stringify(body).substring(0, 2500)}`)
}
