'use strict'

const authHelper = require('helpers/auth.helper')
const moment = require('moment')
const models = require('models')
const responder = require('helpers/response.helper')
const httpContext = require("express-http-context");


const _isTokenValid = (headers) => {
	const apiKey = headers['x-api-key']

	if (apiKey && (apiKey !== undefined)) {
		try {
			const decodedPayload = authHelper.decodeBase64(apiKey);

			if (decodedPayload && (decodedPayload.token !== undefined) && (decodedPayload.expiration > moment().unix())) {
				return decodedPayload.token
			}
		} catch (e) {
			return false
		}
	}
	return undefined
}

exports.validateAccess = (userType = null) => async (req, res, next) => {
	const { headers } = req
	let token;
	if ((token = _isTokenValid(headers)) === false) {
		return responder.respondUnauthorized(res, global.__i18n.__("defaults.INVALID_TOKEN"))
	} else if (token === undefined) {
		return userType.includes("any") ? next() : responder.respondUnauthorized(res, global.__i18n.__("defaults.MISSING_TOKEN"))
	}

	const payload = authHelper.decodeToken(token)
	let model

	if (payload.type === 'admin' && (!userType || userType.includes("admin"))) {
		model = models.admin
	} else if (payload.type === 'user' && (!userType || userType.includes("user"))) {
		model = models.user
	} else {
		return responder.respondUnauthorized(res)
	}


	const user = await model.findById(payload.id)
	if (!user) {
		__logger.error(`validateAccess: Error getting ${payload.type} for ID ${payload.id}`)
		return responder.respondBadRequest(res, global.__i18n.__("defaults.USER_NOT_FOUND"))
	}

	if (payload.type === 'admin') {
		httpContext.set("currentAdmin", user);
	}

	req.data = {
		type: payload.type,
		user: user.get({ plain: true })
	}

	if (payload.type === 'admin' && user.profile == 'operator') {
		req.data.cities = await user.getCities();
	}

	return next()
}
