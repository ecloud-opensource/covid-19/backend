const path = require('path')
const extensions = require('catalogs/extensions')

exports.validateUploadImage = (req, file, cb) => {
	if (!file.originalname.match(extensions.IMAGE_REGEX)) {
		return cb(null, false)
	}
	return cb(null, true)
}

exports.setFileName = (req, file, cb) => {
	cb(null, Date.now() + path.extname(file.originalname))
}
