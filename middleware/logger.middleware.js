'use strict'

const winston = require('winston')

const tsFormat = () => (new Date()).toLocaleTimeString('en-US', {
	month: 'short', day: 'numeric'
})

const logger = new (winston.Logger)({
	transports: [
		new (winston.transports.Console)({
			timestamp: tsFormat,
			colorize: true,
			level: process.env.NODE_ENV === 'development' ? 'silly' : 'info'
		})
	]
})

module.exports = logger
