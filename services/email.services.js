/* eslint-disable global-require */

'use strict'

const sgMail = require('@sendgrid/mail')
const info = "info@cobeat.app"
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendEmailRequest = async (mail, callback) => {
	try {
		const result = await sgMail.send(mail)
		__logger.info('emailServices->sendEmailRequest: Success response received')
		return callback(null, {})
	} catch (error) {
		__logger.error('emailServices->sendEmailRequest: Error response received', error)
		return callback(error, null)
	}
}

exports.sendForgotPassword = async (user, link) => {
	const mail = {
		from: {
			email: __config.sendgrid.fromEmail,
			name: __config.sendgrid.fromName
		},
		to: user.email,
		templateId: __config.sendgrid.templates.forgotPassword,
		attachments: [],
		dynamic_template_data: {
			link,
			email: user.email || user.primaryEmail,
			name: user.name
		}
	}
	// Send email service
	sendEmailRequest(mail, (err, response) => {
		if (err) {
			__logger.error(`emailServices->sendForgotPassword: Error sending email to ${user.email}`, err)
			return Promise.reject(err)
		}
		return Promise.resolve(response)
	})
}


exports.sendWelcomeEmail = async (user) => {
	const mail = {
		from: {
			email: __config.sendgrid.fromEmail,
			name: __config.sendgrid.fromName
		},
		to: user.email,
		templateId: __config.sendgrid.templates.welcomeEmail,
		attachments: []
	}

	// Send email service
	sendEmailRequest(mail, (err, response) => {
		if (err) {
			__logger.error(`emailServices->sendForgotPassword: Error sending email to ${user.email}`, err)
			return Promise.reject(err)
		}
		return Promise.resolve(response)
	})
}

exports.sendContactEmail = async (user) => {
	return new Promise((resolve, reject) => {
		const mail = {
			from: {
				email: __config.sendgrid.fromEmail,
				name: __config.sendgrid.fromName
			},
			to: info,
			templateId: __config.sendgrid.templates.contactEmail,
			dynamic_template_data: {
				email: user.email,
				name: user.firstName + " " + user.lastName,
				message: user.message
			}
		}

		// Send email service
		sendEmailRequest(mail, (err, response) => {
			if (err) {
				__logger.error(`emailServices->sendForgotPassword: Error sending email to ${user.email}`, err)
				return reject(err)
			}
			return resolve(response)
		})
	})
}

