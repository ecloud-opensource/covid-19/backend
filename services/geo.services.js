const models = require("models");
const {
  reverseGeocoding,
  geocoding
} = require("services/google_api.services")

const {
  buildPointFromCoordinates,
  buildPolygonFromNESW,
  buildPointSQLFromCoordinates,
  buildPolygonSQLFromBounds
} = require("helpers/geo.helper")


exports.findPlace = async (point) => new Promise(async (resolve, reject) => {
  try {
    let neighborhood, city, state, country, results;

    const queryCity = {
      where:
        models.sequelize.and(
          models.sequelize.fn(
            `ST_DWithin`,
            buildPointSQLFromCoordinates(point.coordinates[0], point.coordinates[1]),
            models.sequelize.col(`"city"."area"`),
            0
          )
        ),
      include: [
        {
          model: models.state,
          as: 'state',
          include: {
            model: models.country,
            as: 'country'
          }
        }
      ]
    };

    const queryNeighborhood = {
      where: models.sequelize.and(
        models.sequelize.fn(
          `ST_DWithin`,
          buildPointSQLFromCoordinates(point.coordinates[0], point.coordinates[1]),
          models.sequelize.col(`"neighborhood"."area"`),
          0
        )
      )
    };

    [city, neighborhood] = await Promise.all([
      models.city.findOne(queryCity),
      models.neighborhood.findOne(queryNeighborhood)
    ]);

    if (!city) {

      results = await reverseGeocoding(point.coordinates);

      const result = results.filter((item) => item.geometry.location_type === "APPROXIMATE" &&
        item.address_components.find((item) => item.types.includes("country")) &&
        item.address_components.find((item) => item.types.includes("locality")) &&
        item.address_components.find((item) => item.types.includes("administrative_area_level_1"))
      )

      if (!result.length) {
        throw new Error("No se puedo recuperar el lugar");
      }

      country = result[0].address_components.filter((item) => item.types.includes("country"))[0];

      state = result[0].address_components.filter((item) => item.types.includes("administrative_area_level_1"))[0];

      city = result[0].address_components.filter((item) => item.types.includes("locality"))[0];

      neighborhood = result[0].address_components.filter((item) => item.types.includes("neighborhood"))[0];

      if (!country) {

        throw new Error("No se encontro " + (country ? "el pais" : "la ciudad ") + " de la quarentena");

      } else {

        [country, countryCreated] = await models.country.findOrCreate({
          where: { code: country.long_name },
          defaults: { name: country.long_name },
          plain: true
        });

        if (countryCreated) {

          const geoData = await getGeoData(
            await geocoding(country.code)
          );

          country.set('center', geoData.point);

          country.set('area', geoData.polygon);

          country.save();
        }

        state.long_name = state.long_name.replace(" Province", "");

        [state, stateCreated] = await models.state.findOrCreate({
          where: { code: state.long_name },
          defaults: { name: state.long_name },
          plain: true
        });

        if (stateCreated) {

          const geoData = await getGeoData(
            await geocoding(`${state.code},${country.code}`)
          );

          state.set('center', geoData.point);

          state.set('area', geoData.polygon);

          state.set('idCountry', country.id);


          state.save();
        }

        [city, cityCreated] = await models.city.findOrCreate({
          where: { code: city.long_name },
          defaults: { name: city.long_name },
          plain: true
        });

        if (cityCreated) {
          const geoData = await getGeoData(await
            geocoding(`${city.code},${state.code},${country.code}`));

          city.set('center', geoData.point);

          city.set('area', geoData.polygon);

          city.set('idState', state.id);

          city.set('idCountry', country.id);


          city.save();
        }
      }
    } else {

      state = city.state;
      country = city.state.country

    }

    if (!neighborhood) {


      results = results ? results : await reverseGeocoding(point.coordinates);

      const result = results.filter((item) => item.geometry.location_type === "APPROXIMATE" &&
        (item.address_components.find((item) => item.types.includes("neighborhood")) ||
          item.address_components.find((item) => item.types.includes("sublocality")))
      )

      if (result.length) {

        neighborhood = result[0].address_components.filter((item) => item.types.includes("neighborhood")
          || item.types.includes("sublocality"))[0];

        [neighborhood, neighborhoodCreated] = await models.neighborhood.findOrCreate({
          where: { code: neighborhood.long_name },
          defaults: { name: neighborhood.long_name },
          plain: true
        });

        if (neighborhoodCreated) {
          const geoData = await getGeoData(
            await geocoding(`${neighborhood.code},${city.code},${state.code},${country.code}`)
          );


          neighborhood.set('center', geoData.point);

          neighborhood.set('area', geoData.polygon);

          neighborhood.set('idCity', city.id);

          neighborhood.set('idCountry', country.id);

          neighborhood.save();

        }

      }
    }

    return resolve({ neighborhood, city, state, country });

  } catch (err) {
    __logger.info(`geoServices->findPlace: Error ${err}`)
    return reject(err);
  }
});


exports.getUserMap = async (bounds, user) => new Promise((resolve, reject) => {
  try {
    return resolve(models.user.findAll({
      where: models.sequelize.and(
        models.sequelize.fn(
          "ST_CONTAINS",
          buildPolygonSQLFromBounds(
            bounds.bottomLeft.coordinates,
            bounds.topLeft.coordinates,
            bounds.topRight.coordinates,
            bounds.bottomRight.coordinates),
          models.sequelize.literal('"latestPosition"."position"'),
        ),
        { id: { $ne: user.id }, idCity: user.idCity }
      ),
      attributes: ["isQuarantined"],
      include: {
        model: models.position,
        as: 'latestPosition',
        attributes: ['position']
      }
    }))
  } catch (err) {
    __logger.info(`geoServices->getUserMap: Error ${err}`)
    return reject(err);
  }
})


exports.getAdminMap = async (bounds, status) => new Promise((resolve, reject) => {
  try {
    return resolve(models.user.findAll({
      where: models.sequelize.and(
        models.sequelize.fn(
          "ST_CONTAINS",
          buildPolygonSQLFromBounds(
            bounds.bottomLeft.coordinates,
            bounds.topLeft.coordinates,
            bounds.topRight.coordinates,
            bounds.bottomRight.coordinates),
          models.sequelize.literal('"latestPosition"."position"'),
        ),
        status ? { status } : {}
      ),
      attributes: ["isQuarantined", "id", "status", "onMovement", "outOfSafehouse"],
      include: {
        model: models.position,
        as: 'latestPosition',
        attributes: ['position']
      }
    }))
  } catch (err) {
    __logger.info(`geoServices->getUserMap: Error ${err}`)
    return reject(err);
  }
})

exports.nearQuarentineUser = async (point, user) => new Promise((resolve, reject) => {
  try {
    return resolve(models.user.count({
      where: models.sequelize.and(
        models.sequelize.fn(`${__config.maxDistanceOfAPersonWithQuarantine} >=`,
          models.sequelize.fn(
            "ST_Distance_Sphere",
            buildPointSQLFromCoordinates(point.coordinates[0], point.coordinates[1]),
            models.sequelize.literal('"latestPosition"."position"')),
        ),
        { isDeleted: false },
        { isQuarantined: true },
        { id: { $ne: user.id } }
      ),
      include: {
        model: models.position,
        as: 'latestPosition',
        attributes: ['position']
      }
    }))

  } catch (err) {
    __logger.info(`geoServices->nearQuarentineUser: Error ${err}`)
    return reject(err);
  }
})

exports.nearUser = async (point, user) => new Promise((resolve, reject) => {
  try {
    return resolve(models.user.findAll({
      where: models.sequelize.and(
        models.sequelize.fn(`${__config.maxDistanceOfAPersonMatch} >=`,
          models.sequelize.fn(
            "ST_Distance_Sphere",
            buildPointSQLFromCoordinates(point.coordinates[0], point.coordinates[1]),
            models.sequelize.literal('"latestPosition"."position"')),
        ),
        { isDeleted: false },
        { id: { $ne: user.id } },
      ),
      attributes: [
        "isQuarantined",
        ["id", "idUser"],
        "status",
        "onMovement",
        "outOfSafehouse",
        [
          models.sequelize.fn(
            "ST_Distance_Sphere",
            buildPointSQLFromCoordinates(point.coordinates[0], point.coordinates[1]),
            models.sequelize.literal('"latestPosition"."position"'))
          , "distance"]
      ],
      include: {
        model: models.position,
        as: 'latestPosition',
        attributes: ['position']
      }
    }))

  } catch (err) {
    __logger.info(`geoServices->nearQuarentineUser: Error ${err}`)
    return reject(err);
  }
})

exports.getInfected = (options) => {
  return new Promise(async (resolve, reject) => {
    try {

      models.neighborhood.findOne({ where: { id: options.idNeighborhood }, attributes: ["areInfected"] })
        .then(async (neighborhood) => {
          if (!neighborhood || !neighborhood.areInfected) {
            return await models.city.findOne({ where: { id: options.idCity }, attributes: ["areInfected"] })
          }
          return resolve(global.__i18n.__("geo.notifications.quarantinedNeighborhood", neighborhood.areInfected))

        }).then(async (city) => {
          if (!city || !city.areInfected) {
            return await models.state.findOne({ where: { id: options.idState }, attributes: ["areInfected"] })
          }
          return resolve(global.__i18n.__("geo.notifications.quarantinedCity", city.areInfected))
        }).then(async (state) => {
          if (!state || !state.areInfected) {
            return await models.country.findOne({ where: { id: options.idCountry }, attributes: ["areInfected"] })
          }
          return resolve(global.__i18n.__("geo.notifications.quarantinedState", state.areInfected))
        }).then((country) => {
          if (country) {
            return resolve(global.__i18n.__("geo.notifications.quarantinedCountry", country.areInfected))
          } else {
            return resolve(global.__i18n.__("geo.notifications.zeroquarantined"))
          }
        })

    } catch (e) {
      return reject(e)
    }
  })
}

const getGeoData = (results) => new Promise((resolve, reject) => {
  try {

    const result = results[0];

    return resolve(
      {
        point: buildPointFromCoordinates(result.geometry.location),
        polygon: buildPolygonFromNESW(result.geometry.viewport.northeast, result.geometry.viewport.southwest)
      }
    )

  } catch (err) {
    __logger.info(`geoServices->getGeoData: Error ${err}`)
    return reject(err);
  }
})

