const axios = require("axios");
const models = require("../models");

const reverseGeocodingURL = `https://maps.googleapis.com/maps/api/geocode/json?key=${process.env.GOOGLE_API_TOKEN}`
const geocodingURL = `https://maps.googleapis.com/maps/api/geocode/json?key=${process.env.GOOGLE_API_TOKEN}`


exports.reverseGeocoding = async (coordinates) => new Promise(async (resolve, reject) => {
  try {

    const requestURL = `${reverseGeocodingURL}&latlng=${coordinates[0]},${coordinates[1]}`;

    const response = await axios.get(requestURL);

    if (response.status === 200 && response.data.status === "OK") {

      return resolve(response.data.results);

    } else {
      throw new Error("GMaps no respondio correctamente.")
    }

  } catch (err) {
    __logger.info(`googleApiServices->reverseGeocoding: Error ${err}`)
    return reject(err);
  }
});

exports.geocoding = async (place) => new Promise(async (resolve, reject) => {

  try {
    const requestURL = encodeURI(`${geocodingURL}&address=${place}`);

    const response = await axios.get(requestURL);

    if (response.status === 200 && response.data.status === "OK") {

      return resolve(response.data.results);
    } else {
      throw new Error("GMaps no respondio correctamente.")
    }

  } catch (err) {
    __logger.info(`googleApiServices->geocoding: Error ${err}`)
    return reject(err);
  }
})


