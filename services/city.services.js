const models = require("models")

exports.getLinks = (idCity, idCountry) => {
    return new Promise(async (resolve, reject) => {
        try {

            let country = await models.country.findOne({
                attributes: ["id", "name"],
                where: { id: idCountry },
                include: {
                    model: models.link,
                    as: "link"
                }
            });

            let city = await models.city.findOne({
                attributes: ["id", "name"],
                where: { id: idCity },
                include: {
                    model: models.link,
                    as: "link"
                }
            });

            country = country ? country : {};
            city = city ? city : {};

            return resolve({ country, city })

        } catch (e) {
            return reject(e)
        }
    })
}

