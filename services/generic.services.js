const models = require('models')
const httpContext = require("express-http-context");

exports.GetAll = async (modelType, options, customData = {}) => new Promise(
	async (resolve, reject) => {
		try {
			__logger.info(`genericServices->findAll: Retrieving model: ${modelType}`)

			const registries = await models[modelType].scope(options.scopes).findAll({
				...options.query,
				...customData
			})

			resolve({
				rows: registries,
				metadata: {
					totalResults: options.pageSize ? await models[modelType].scope(options.scope).count({
						...options.query,
						limit: 0,
						offset: 0,
						distinct: true,
						col: `${await models[modelType].name}.${await models[modelType].rawAttributes.id.fieldName}`
					}) : registries.length,
					currentPage: options.page ? options.page : 1,
					pageSize: options.pageSize ? options.pageSize : registries.length
				}
			})
		} catch (e) { reject(new Error(e)) }
	}
)

exports.GetOneBy = async (modelType, where, scopes = ['defaultScope'], customData = {}, required = true) => new Promise(
	async (resolve, reject) => {
		try {
			const result = await models[modelType].scope(scopes).findOne({ where, customData })

			if (!result && required) {
				const error = new Error()
				error.message = global.__i18n.__("defaults.ENTITY_NOT_FOUND")
				return reject(error)
			}

			return resolve(result)
		} catch (e) {
			const error = new Error(e)
			if (error.message === 'TypeError: Cannot read property \'id\' of null') {
				error.message = global.__i18n.__("defaults.ENTITY_NOT_FOUND")
			}
			return reject(error)
		}
	}
)

exports.Update = async (modelType, updates, where, options = {}) => new Promise(
	async (resolve, reject) => {
		try {
			const currentAdmin = httpContext.get("currentAdmin");
			options.customData = {};
			if (currentAdmin) {
				options.customData.currentAdmin = currentAdmin;
			}

			let updated = (await models[modelType].update(updates, { where, ...options, returning: true }))[1][0];

			updated = updated ? updated : {};

			return resolve(updated)
		} catch (e) { reject(new Error(e)) }
	}
)

exports.Create = async (modelType, entity, options = {}) => new Promise(
	async (resolve, reject) => {
		try {
			const currentAdmin = httpContext.get("currentAdmin");
			options.customData = {};
			if (currentAdmin) {
				options.customData.currentAdmin = currentAdmin;
				if (options.include) {
					for (const model of options.include) {
						model.customData = {};
						model.customData.currentAdmin = currentAdmin;
					}
				}
			}

			return resolve(await models[modelType].create(entity, options))
		} catch (e) { reject(new Error(e)) }
	}
)

exports.BulkCreate = async (modelType, entity, options = {}) => new Promise(
	async (resolve, reject) => {
		try {
			options.customData = {}
			return resolve(await models[modelType].bulkCreate(entity, options))
		} catch (e) { reject(new Error(e)) }
	}
)

exports.Delete = async (modelType, where, options = {}) => new Promise(
	async (resolve, reject) => {
		try {
			resolve(await models[modelType].update({ isDeleted: true }, { where, ...options }))
		} catch (e) { reject(new Error(e)) }
	}
)

exports.Count = async (modelType, where, options = {}) => new Promise(
	async (resolve, reject) => {
		try {
			resolve(await models[modelType].count({ where, ...options }));
		} catch (e) { reject(new Error(e)) }
	}
)

