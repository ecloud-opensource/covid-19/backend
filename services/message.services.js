const { formatSMSCPhoneNumber } = require("../helpers/numeric.helper");
const axios = require("axios");
const twilio = require("twilio")
const twilioClient = () => twilio(process.env.TWILIO_SID, process.env.TWILIO_TOKEN);

exports.sendSMSViaSMSMasivos = async (phone, code) => new Promise(async (resolve, reject) => {
  try {
    __logger.info("Using SMSMASIVOS");

    const url = `http://servicio.smsmasivos.com.ar/enviar_sms.asp?api=1&usuario=${process.env.SMSMASIVOS_USERNAME}&clave=` +
      `${process.env.SMSMASIVOS_PASSWORD}&tos=${formatSMSCPhoneNumber(phone)}` +
      `& texto=${global.__i18n.__("messaging.smsMasivosCode", __config.clientName, code.toString())
      }`;

    const response = await axios.get(url);

    if (response.data.status === "OK") {
      return resolve(true);
    } else {
      return reject(err);
    }
  } catch (err) {
    __logger.info(`geoServices->sendSMSViaSMSMasivos: Error ${err}`);
    return reject(err);
  }
});

exports.sendSMSViaWhatsApp = async (phone, code) => new Promise(async (resolve, reject) => {
  try {
    __logger.info("Using WhatsApp");

    const url = `https://whatzmeapi.com:10501/rest/api/enviar-mensaje?token=${process.env.WHATZME_TOKEN}`;

    const body = {
      mensaje: global.__i18n.__("messaging.whatsAppCode", __config.clientName, code.toString()),
      numero: phone
    }
    const response = await axios.post(url, body);

    if (response.status === 200 && response.data.exito) {
      return resolve(true);
    } else {
      return reject(response.mensajeError);
    }
  } catch (err) {
    __logger.info(`geoServices->sendSMSViaWhatsApp: Error ${err}`);
    return reject(false);
  }
});

exports.sendSMSViaTwilio = async (phone, code) => new Promise((resolve, reject) => {
  try {
    __logger.info("Using Twilio");
    twilioClient().messages.create({
      body: global.__i18n.__("messaging.twilioCode", __config.clientName, code.toString()),
      from: __config.twilio.fromSMSPhone,
      to: phone
    }).then(() => resolve(true)).catch((err) => reject(err));
  } catch (err) {
    __logger.info(`geoServices->sendSMSViTwilio: Error ${err}`);
    return reject(err);
  }
});
