module.exports = {
	extends: [
		'airbnb-base'
	],
	plugins: [
		'import',
	],
	rules: {
		'indent': ['error', 'tab', { "SwitchCase": 1 }],
		'no-tabs': 0,
		'strict': 0,
		'semi': ['error', 'never'],
		'comma-dangle': ['error', 'never'],
		'no-restricted-syntax': 0,
		'no-underscore-dangle': 0,
		'arrow-parens': 0,
		'radix': 0,
		'new-cap': 0,
		'new-parens': 0,
		'no-unused-expressions': 0,
		'no-await-in-loop': 0,
		'no-async-promise-executor': 0,
		'no-param-reassign': ["error", { "props": false }]
	},
	ignorePatterns: ["node_modules/"],
	settings: {
		'import/resolver': {
			'node': {
				'moduleDirectory': [
					'node_modules',
					'.'
				]
			}
		}
	},
	globals: {
		__config: "writable",
		__logger: "writable"
	}
};