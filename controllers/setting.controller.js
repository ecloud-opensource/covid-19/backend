'use strict'

const {
  GetAll,
  GetOneBy,
  Create,
  Update,
  Delete
} = require('services/generic.services')
const { respondData, respondOperationFailed } = require('helpers/response.helper')

/**
 * @api {GET} /setting Get settings
 * @apiName GetSettings
 * @apiGroup Setting
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[[setting]](#api-Modelos-ObjectSetting)} data.rows[] Array of settings
 * @apiUse MetaDataResponse
 */
exports.getAll = async (req, res) => {
  req.queryOptions.addSearchTermColumns(['name'])
  const setting = await GetAll('setting', req.queryOptions)
  return respondData(res, setting)
}

/**
 * @api {GET} /setting/:code Get single setting
 * @apiName GetSetting
 * @apiGroup Setting
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Setting ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[setting](#api-Modelos-ObjectSetting)} data Setting
 */
exports.getOneById = async (req, res) => respondData(
  res, await GetOneBy('setting', { code: req.params.code }, req.queryOptions.scopes)
)

/**
 * @api {PUT} /setting/:code Update setting
 * @apiName UpdateSetting
 * @apiGroup Setting
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Setting ID
 * @apiUse SettingModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[setting](#api-Modelos-ObjectSetting)} data Setting
 */
exports.update = async (req, res) => {
  return respondData(res, await Update(
    'setting',
    req.body,
    { id: req.params.id }
  ))
}

/**
 * @api {GET} /setting/terms-and-conditions Get terms and conditions
 * @apiName GetTermsAndConditions
 * @apiGroup Setting
 * @apiVersion 1.0.0
 *
 * @apiUse SuccessResponse
 * @apiSuccess {String} data.url Url of terms and conditions
 */
exports.termsAndConditions = async (req, res) => {
  try {

    let termsAndConditions = __config.clientUrl

    const setting = await GetOneBy('setting', { code: "TERMS_AND_CONDITIONS" }, req.queryOptions.scopes, {}, false);

    if (setting) {

      termsAndConditions = setting.value

      if (req.data && req.data.user && req.data.user.safehouse) {

        const city = await GetOneBy('city', { id: req.data.user.idCity }, req.queryOptions.scopes, {}, false);

        if (city.termsAndConditions) {

          termsAndConditions = city.termsAndConditions
        }

        else {


          const country = await GetOneBy('country', { id: req.data.user.idCountry }, req.queryOptions.scopes, {}, false);

          if (country.termsAndConditions) {

            termsAndConditions = country.termsAndConditions
          }

        }
      }
    }


    return respondData(res, { url: termsAndConditions });

  } catch (e) {
    return respondOperationFailed(res, e);
  }
}


