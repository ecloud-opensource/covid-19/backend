'use strict'

const {
	GetAll,
	GetOneBy,
	Create,
	Update,
	Delete
} = require('services/generic.services')

const { respondData } = require('helpers/response.helper')

/**
 * @api {GET} /links Get links
 * @apiName GetLinks
 * @apiGroup Link
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[link[]]](#api-Modelos-ObjectLink)} data.rows[] Array of links
 * @apiUse MetaDataResponse
 */
exports.getAll = async (req, res) => {
	req.queryOptions.addSearchTermColumns(['title'])
	const links = await GetAll('link', req.queryOptions)
	return respondData(res, links)
}

/**
 * @api {GET} /links/:id Get single link
 * @apiName GetLink
 * @apiGroup Link
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Link ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[link](#api-Modelos-ObjectLink)} data Link
 */
exports.getOneById = async (req, res) => respondData(
	res, await GetOneBy('link', { id: req.params.id }, req.queryOptions.scopes)
)

/**
 * @api {PUT} /links/:id Update link
 * @apiName UpdateLink
 * @apiGroup Link
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Link ID
 * @apiUse LinkModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[link](#api-Modelos-ObjectLink)} data Link
 */
exports.update = async (req, res) => respondData(
	res, await Update(
		'link',
		req.body,
		{ id: req.params.id }
	)
)


/**
 * @api {DELETE} /links/:id Delete link
 * @apiName DeleteLink
 * @apiGroup Link
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Link ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[link](#api-Modelos-ObjectLink)} data Link
 */
exports.deleteEntity = async (req, res) => respondData(
	res, await Delete(
		'link',
		{ id: req.params.id }
	)
)


/**
 * @api {POST} /links Create link
 * @apiName CreateLink
 * @apiGroup Link
 * @apiVersion 1.0.0
 *
 * @apiUse LinkModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[link](#api-Modelos-ObjectLink)} data Link
 */
exports.create = async (req, res) => respondData(
	res, await Create('link', req.body)
)
