'use strict'

const { GetAll, GetOneBy } = require('services/generic.services')
const { respondData, respondBadRequest } = require('helpers/response.helper')
const statuses = require("catalogs/statuses")
const models = require("models")
const sequelize = require("models").sequelize
const moment = require("moment")

/**
 * @api {GET} /dashboard/main Get main dashboard
 * @apiName MainDashboard
 * @apiGroup Dashboard
 * @apiVersion 1.0.0
 *
 * 
 * @apiParam {Integer} idCity City ID
 * @apiParam {Integer} idCountry Country ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {Number} data.misbehavior.HEALTHY Healthy fellows
 * @apiSuccess {Number} data.fellows.HEALTHY Healthy fellows
 * @apiSuccess {Number} data.fellows.QUARANTINED Quarantined fellows
 * @apiSuccess {Number} data.fellows.ON_RISK On risk fellows
 * @apiSuccess {Number} data.fellows.EXPOSED Exposed fellows
 * @apiSuccess {Number} data.fellows.WITH_SYMPTOMS Fellows with symptoms
 *

 */
exports.mainDashboard = async (req, res) => {
    const response = {};
    for (const status in statuses.FIELD) {
        response[status] = 0;
    }
    const options = {
        group: ['status'],
        attributes: ['status']
    }
    if (req.query.idCountry) {
        options.where = { idCountry: req.query.idCountry }
    } else if (req.query.idCity) {
        options.where = { idCity: req.query.idCity }
    }

    const [statusesCount, misbehaviorCount] =
        await Promise.all([
            models.user.count(options),
            models.misbehavior.count({ where: options.where })
        ])


    for (const statusCount of statusesCount) {
        if (response.hasOwnProperty(statusCount.status)) {
            response[statusCount.status] = +statusCount.count
        }
    }

    return respondData(res, { fellows: response, misbehavior: misbehaviorCount })
}

/**
 * @api {GET} /dashboard/graph Get dashboard graph
 * @apiName DashboardGraph
 * @apiGroup Dashboard
 * @apiVersion 1.0.0
 *
 *
 * @apiParam {Integer} idCity City ID
 * @apiParam {Integer} idCountry Country ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {Number} data.infected[] 
 * @apiSuccess {Object} data.infected[].count Infected count
 * @apiSuccess {Object} data.infected[].date Infected date
 * @apiSuccess {Object} data.users[] Fellows
 * @apiSuccess {Object} data.users[].date Fellows Date
 * @apiSuccess {Object} data.users[].count Feellows  Count
 *
 */
exports.dashboardGraph = async (req, res) => {
    const promises = [];

    let queryInfected = `SELECT DISTINCT ON (date) "log"."new_value"::integer AS "count", DATE("log"."created_at") AS "date" `;
    let queryInfectedFrom = `FROM "countryLog" AS "log" `;
    let queryInfectedWhere = `WHERE "log"."field" = 'areInfected' `;
    const queryInfectedGroup = `ORDER BY "date" ASC, "log"."created_at" DESC `;

    let queryUsers = `SELECT COUNT("user"."id")::integer AS "count", DATE("user"."created_at") AS "date" FROM "user" `;
    let queryUsersWhere = "";
    const queryUsersGroup = `GROUP BY date ORDER BY date ASC`

    if (req.query.idCountry) {
        queryInfectedWhere += `AND "log"."id_country" = ${req.query.idCountry} `;
        queryUsersWhere = `WHERE "user"."id_country" = ${req.query.idCountry}`;
    } else if (req.query.idCity) {
        queryInfectedFrom = `FROM "cityLog" AS "log" `
        queryInfectedWhere += `AND "log"."id_city" = ${req.query.idCity} `;
        queryUsersWhere = `WHERE "user"."id_city" = ${req.query.idCity} `;
    }

    if (req.query.fromDate) {
        queryInfectedWhere += `AND "log"."created_at" >= '${req.query.fromDate} 00:00:00' `;
        queryUsersWhere += `AND "user"."created_at" >= '${req.query.fromDate} 00:00:00' `;
    }

    if (req.query.toDate) {
        queryInfectedWhere += `AND "log"."created_at" <= '${req.query.toDate} 23:59:59' `;
        queryUsersWhere += `AND "user"."created_at" <= '${req.query.toDate} 23:59:59' `;
    }

    queryInfected = queryInfected + queryInfectedFrom + queryInfectedWhere + queryInfectedGroup;
    queryUsers = queryUsers + queryUsersWhere + queryUsersGroup;

    promises.push(models.sequelize.query(queryInfected, { type: sequelize.QueryTypes.SELECT }));
    promises.push(models.sequelize.query(queryUsers, { type: sequelize.QueryTypes.SELECT }));

    Promise.all(promises).then((results) => {
        const infected = [], users = [];
        const fromDate = req.query.fromDate ? moment(req.query.fromDate) : moment("2020-01-01");
        const daysCount = req.query.toDate ? moment(req.query.toDate).diff(fromDate, "days") : moment().diff(fromDate, "days");

        for (let i = 0; i <= daysCount; i++) {
            const date = moment(fromDate).add(i, "days").format("YYYY-MM-DD");

            const infectedRow = results[0].filter(infected => infected.date === date);
            const userRow = results[1].filter(user => user.date === date);

            infected.push(infectedRow.length ? infectedRow[0] : { count: 0, date} );
            users.push(userRow.length ? userRow[0] : { count: 0, date} );
        }
        return respondData(res, {infected, users});
    });
}