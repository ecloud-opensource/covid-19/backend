'use strict'

const {
	GetAll,
	GetOneBy,
	Create,
	Update,
	Delete
} = require('services/generic.services')

const { respondData, respondBadRequest } = require('helpers/response.helper')
const { getLinks } = require("services/city.services")

/**
 * @api {GET} /city Get cities
 * @apiName GetCities
 * @apiGroup City
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[[city]](#api-Modelos-ObjectCity)} data.rows[] Array of cities
 * @apiUse MetaDataResponse
 */
exports.getAll = async (req, res) => {
	req.queryOptions.addSearchTermColumns(['name'])
	if (req.data.user.profile === "operator") {
		req.queryOptions.addCustomAnd(`"city"."id" IN (${req.data.cities.map((item) => item.id)}) `)
	}
	const cities = await GetAll('city', req.queryOptions)
	return respondData(res, cities)
}

/**
 * @api {GET} /city/:id Get single city
 * @apiName GetCity
 * @apiGroup City
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id City ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[city](#api-Modelos-ObjectCity)} data City
 */
exports.getOneById = async (req, res) => respondData(
	res, await GetOneBy('city', { id: req.params.id }, req.queryOptions.scopes)
)

/**
 * @api {GET} /city/mobile Get cities for mobile
 * @apiName GetCitiesMobile
 * @apiGroup City
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[[city]](#api-Modelos-ObjectCity)} data.city.rows[] Array of cities
 * @apiUse MetaDataResponse
 */
exports.getAllMobile = async (req, res) => {
	req.queryOptions.addSearchTermColumns(['name'])
	const cities = await GetAll('city', req.queryOptions, {
		attributes: ['fellows', 'areInfected', 'name', 'id']
	})
	return respondData(res, cities)
}

/**
 * @api {GET} /city/mobile/:id Get single city for mobile
 * @apiName GetCityMobile
 * @apiGroup City
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id City ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[city](#api-Modelos-ObjectCity)} data.city City
 */
exports.getOneByIdMobile = async (req, res) => respondData(
	res, {
	city: await GetOneBy('city', { id: req.params.id }, req.queryOptions.scopes,
		{ attributes: ['fellows', 'areInfected', 'name', 'id'] })
}
)


/**
 * @api {GET} /city/feed Get city feed
 * @apiName GetCityFeed
 * @apiGroup City
 * @apiVersion 1.0.0
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[cityFeed](#api-Modelos-ObjectCityfeed)} data CityFeed
 */
exports.getFeed = async (req, res) => {
	return respondData(res, await GetAll('cityFeed', { idCity: req.data.user.idCity }))
}

/**
 * @api {GET} /city/links Get city links
 * @apiName GetCityLinks
 * @apiGroup City
 * @apiVersion 1.0.0
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[city](#api-Modelos-ObjectCity)} data.city City
 * @apiSuccess {[country](#api-Modelos-ObjectCountry)} data.country Country
 */
exports.getLinks = async (req, res) => {
	return respondData(res, await getLinks(req.data.user.idCity, req.data.user.idCountry))
}

/**
 * @api {GET} /city/log Get city log for admin
 * @apiName GetCityLog
 * @apiGroup City
 * @apiVersion 1.0.0
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[cityLog](#api-Modelos-ObjectCityLog)} data.cityLog City log
 */
exports.getLog = async (req, res) => {
	const cityLog = await GetAll('cityLog', req.queryOptions)
	return respondData(res, cityLog)
}

/**
 * @api {PUT} /city/:id Update city
 * @apiName UpdateCity
 * @apiGroup City
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id City ID
 * @apiUse CityModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[city](#api-Modelos-ObjectCity)} data City
 */
exports.update = async (req, res) => {
	return respondData(res, await Update(
		'city',
		req.body,
		{ id: req.params.id }
	))
}

/**
 * @api {DELETE} /city/:id Delete city
 * @apiName DeleteCity
 * @apiGroup City
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id City ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[city](#api-Modelos-ObjectCity)} data City
 */
exports.deleteEntity = async (req, res) => respondData(
	res, await Delete(
		'city',
		{ id: req.params.id }
	)
)


/**
 * @api {POST} /city Create city
 * @apiName CreateCity
 * @apiGroup City
 * @apiVersion 1.0.0
 *
 * @apiUse CityModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[city](#api-Modelos-ObjectCity)} data City
 */
exports.create = async (req, res) => respondData(
	res, await Create('city', { ...req.body })
)

/**
 * @api {GET} /city/ranking Get Ranking for citys
 * @apiName Ranking
 * @apiGroup City
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {Array} data.ranking Ranking of cities
 * @apiSuccess {Number} data.ranking.fellows Fellows
 * @apiSuccess {Number} data.ranking.name Cities name
 * @apiSuccess {Number} data.ranking.id Cities ID
 * @apiSuccess {Number} data.ranking.areInfected Are infected
 *
 * @apiUse MetaDataResponse
 */
exports.ranking = async (req, res) => {
	req.queryOptions.addSearchTermColumns(['name'])
	const cities = await GetAll('city', req.queryOptions, {
		attributes: ['fellows', 'areInfected', 'name', 'id']
	})
	return respondData(res, cities)
}

/**
 * @api {GET} /city/admin-ranking Get Admin Ranking for cities
 * @apiName AdminRanking
 * @apiGroup City
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {Array} data.ranking Ranking of cities
 * @apiSuccess {Number} data.ranking.fellows Fellows
 * @apiSuccess {Number} data.ranking.name Neighborhood name
 * @apiSuccess {Number} data.ranking.id Neighborhood ID
 * @apiSuccess {Number} data.ranking.areInfected Are infected
 * @apiSuccess {Number} data.ranking.areQuarantined Are Quarantined
 * @apiSuccess {Number} data.ranking.areOnRisk Are On Risk
 * @apiSuccess {Number} data.ranking.areExposed Are Exposed
 * @apiSuccess {Number} data.ranking.areWithSymptoms Are With Symptoms
 * @apiSuccess {Number} data.ranking.areHealthy Are Healthy
 *
 *
 *
 * @apiUse MetaDataResponse
 */
exports.adminRanking = async (req, res) => {
	req.queryOptions.addSearchTermColumns(['name'])
	const cities = await GetAll('city', req.queryOptions, {
		attributes: [
			'fellows',
			'areInfected',
			'name',
			'id',
			'areQuarantined',
			'areOnRisk',
			'areExposed',
			'areWithSymptoms',
			'areHealthy'
		]
	})
	return respondData(res, cities)
}
