'use strict'

const responder = require('helpers/response.helper')

/**
 * @api {POST} /upload/image Upload Image
 * @apiName UploadImage
 * @apiGroup Upload
 * @apiVersion 1.0.0
 *
 * @apiParam {String} type Type ['AVATAR']
 * @apiParam {file} file Archivo
 *
 * @apiUse SuccessResponse
 * @apiSuccess {String} data URL of uploaded image
 */
exports.uploadImage = async (req, res) => {
	if (!req.file) {
		return responder.respondError(res, global.__i18n.__("defaults.SET_DATA_FAILED"))
	}
	return responder.respondData(res, { path: `${__config.cdnUrl}/img/${req.file.filename}` })
}
