'use strict'

const {
	GetAll,
	GetOneBy,
	Create,
	Update,
	Delete
} = require('services/generic.services')

const { respondData } = require('helpers/response.helper')

/**
 * @api {GET} /zone Get zones
 * @apiName GetZones
 * @apiGroup Zone
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[[zone]](#api-Modelos-ObjectZone)} data.rows[] Array of zones
 * @apiUse MetaDataResponse
 */
exports.getAll = async (req, res) => {
	req.queryOptions.addSearchTermColumns(['name'])
	const zones = await GetAll('zone', req.queryOptions)
	return respondData(res, zones)
}

/**
 * @api {GET} /zone/:id Get single zone
 * @apiName GetZone
 * @apiGroup Zone
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Zone ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[zone](#api-Modelos-ObjectZone)} data Zone
 */
exports.getOneById = async (req, res) => respondData(
	res, await GetOneBy('zone', { id: req.params.id }, req.queryOptions.scopes)
)

/**
 * @api {PUT} /zone/:id Update zone
 * @apiName UpdateZone
 * @apiGroup Zone
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Zone ID
 * @apiUse ZoneModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[zone](#api-Modelos-ObjectZone)} data Zone
 */
exports.update = async (req, res) => respondData(
	res, await Update(
		'zone',
		req.body,
		{ id: req.params.id }
	)
)


/**
 * @api {DELETE} /zone/:id Delete zone
 * @apiName DeleteZone
 * @apiGroup Zone
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Zone ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[zone](#api-Modelos-ObjectZone)} data Zone
 */
exports.deleteEntity = async (req, res) => respondData(
	res, await Delete(
		'zone',
		{ id: req.params.id }
	)
)


/**
 * @api {POST} /zone Create zone
 * @apiName CreateZone
 * @apiGroup Zone
 * @apiVersion 1.0.0
 *
 * @apiUse ZoneModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[zone](#api-Modelos-ObjectZone)} data Zone
 */
exports.create = async (req, res) => respondData(
	res, await Create('zone', { ...req.body })
)
