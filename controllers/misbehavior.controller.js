'use strict'

const {
  GetAll,
} = require('services/generic.services')

const { respondData } = require('helpers/response.helper')

/**
 * @api {GET} /misbehavior Get misbehaviors
 * @apiName GetMisbehaviors
 * @apiGroup Misbehavior
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[[misbehavior]](#api-Modelos-ObjectMisbehavior)} data.rows[] Array of misbehaviors
 * @apiUse MetaDataResponse
 */
exports.getAll = async (req, res) => {
  const misbehavior = await GetAll('misbehavior', req.queryOptions)
  return respondData(res, misbehavior)
}
