'use strict'

const {
	GetAll,
	GetOneBy,
	Create,
	Update,
	Delete
} = require('services/generic.services')

const { respondData } = require('helpers/response.helper')

/**
 * @api {GET} /city-feed Get city feeds
 * @apiName GetCityFeeds
 * @apiGroup CityFeed
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[[cityFeed]](#api-Modelos-ObjectCityFeed)} data.rows[] Array of city feeds
 * @apiUse MetaDataResponse
 */
exports.getAll = async (req, res) => {
	req.queryOptions.addSearchTermColumns(['title', 'description'])
	const cityFeeds = await GetAll('cityFeed', req.queryOptions)
	return respondData(res, cityFeeds)
}

/**
 * @api {GET} /city-feed/:id Get single city feed
 * @apiName GetCityFeed
 * @apiGroup CityFeed
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id City feed ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[cityFeed](#api-Modelos-ObjectCityFeed)} data City feed
 */
exports.getOneById = async (req, res) => respondData(
	res, await GetOneBy('cityFeed', { id: req.params.id }, req.queryOptions.scopes)
)

/**
 * @api {PUT} /city-feed/:id Update city feed
 * @apiName UpdateCityFeed
 * @apiGroup CityFeed
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id City feed ID
 * @apiUse CityFeedModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[cityFeed](#api-Modelos-ObjectCityFeed)} data City feed
 */
exports.update = async (req, res) => {
	return respondData(res, await Update(
		'cityFeed',
		req.body,
		{ id: req.params.id }
	))
}

/**
 * @api {DELETE} /city-feed/:id Delete city feed
 * @apiName DeleteCityFeed
 * @apiGroup CityFeed
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id City feed ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[cityFeed](#api-Modelos-ObjectCityFeed)} data City feed
 */
exports.deleteEntity = async (req, res) => respondData(
	res, await Delete(
		'cityFeed',
		{ id: req.params.id }
	)
)


/**
 * @api {POST} /city-feed Create city feed
 * @apiName CreateCityFeed
 * @apiGroup CityFeed
 * @apiVersion 1.0.0
 *
 * @apiUse CityFeedModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[cityFeed](#api-Modelos-ObjectCityFeed)} data City feed
 */
exports.create = async (req, res) => respondData(
	res, await Create('cityFeed', { ...req.body})
)
