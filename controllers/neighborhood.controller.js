'use strict'

const {
	GetAll,
	GetOneBy,
	Create,
	Update,
	Delete
} = require('services/generic.services')

const { respondData } = require('helpers/response.helper')

/**
 * @api {GET} /neighborhood Get neighborhoods
 * @apiName GetNeighborhoods
 * @apiGroup Neighborhood
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[neighborhood[]](#api-Modelos-ObjectNeighborhood)} data.rows[] Array of neighborhood
 * @apiUse MetaDataResponse
 */
exports.getAll = async (req, res) => {
	req.queryOptions.addSearchTermColumns(['name'])

	if (req.data.user.profile === "operator") {
		req.queryOptions.addCustomAnd(`"neighborhood"."id_city" IN (${req.data.cities.map((item) => item.id)}) `)
	}
	const neighborhoods = await GetAll('neighborhood', req.queryOptions)
	return respondData(res, neighborhoods)
}

/**
 * @api {GET} /neighborhood/:id Get single neighborhood
 * @apiName GetNeighborhood
 * @apiGroup Neighborhood
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Neighborhood ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[neighborhood](#api-Modelos-ObjectNeighborhood)} data Neighborhood
 */
exports.getOneById = async (req, res) => respondData(
	res, await GetOneBy('neighborhood', { id: req.params.id }, req.queryOptions.scopes)
)


/**
 * @api {GET} /neighborhood/mobile Get neighborhood for mobile
 * @apiName GetNeighborhoods
 * @apiGroup Neighborhood
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[neighborhood[]](#api-Modelos-ObjectNeighborhood)} data.rows[] Array of neighborhood
 * @apiUse MetaDataResponse
 */
exports.getAllMobile = async (req, res) => {
	req.queryOptions.addSearchTermColumns(['name'])
	const neighborhoods = await GetAll('neighborhood', req.queryOptions, {
		attributes: ['fellows', 'areInfected', 'name', 'id']
	})
	return respondData(res, neighborhoods)
}

/**
 * @api {GET} /neighborhood/mobile/:id Get single neighborhood for mobile
 * @apiName GetNeighborhoodMobile
 * @apiGroup Neighborhood
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Neighborhood ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[neighborhood](#api-Modelos-ObjectNeighborhood)} data.neighborhood Neighborhood
 */
exports.getOneByIdMobile = async (req, res) => respondData(
	res, {
	neighborhood: await GetOneBy('neighborhood', { id: req.params.id }, req.queryOptions.scopes,
		{ attributes: ['fellows', 'areInfected', 'name', 'id'] })
}
)

/**
 * @api {GET} /neighborhood/log Get neighborhood log for admin
 * @apiName GetNeighborhoodLog
 * @apiGroup Neighborhood
 * @apiVersion 1.0.0
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[neighborhoodLog](#api-Modelos-ObjectNeighborhoodLog)} data.neighborhoodLog Neighborhood log
 */
exports.getLog = async (req, res) => {
	const neighborhoodLog = await GetAll('neighborhoodLog', req.queryOptions)
	return respondData(res, neighborhoodLog)
}

/**
 * @api {PUT} /neighborhood/:id Update neighborhood
 * @apiName UpdateNeighborhood
 * @apiGroup Neighborhood
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Neighborhood ID
 * @apiUse NeighborhoodModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[neighborhood](#api-Modelos-ObjectNeighborhood)} data Neighborhood
 */
exports.update = async (req, res) => respondData(
	res, await Update(
		'neighborhood',
		req.body,
		{ id: req.params.id }
	)
)


/**
 * @api {DELETE} /neighborhood/:id Delete neighborhood
 * @apiName DeleteNeighborhood
 * @apiGroup Neighborhood
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Neighborhood ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[neighborhood](#api-Modelos-ObjectNeighborhood)} data Neighborhood
 */
exports.deleteEntity = async (req, res) => respondData(
	res, await Delete(
		'neighborhood',
		{ id: req.params.id }
	)
)


/**
 * @api {POST} /neighborhood Create neighborhood
 * @apiName CreateNeighborhood
 * @apiGroup Neighborhood
 * @apiVersion 1.0.0
 *
 * @apiUse NeighborhoodModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[neighborhood](#api-Modelos-ObjectNeighborhood)} data Neighborhood
 */
exports.create = async (req, res) => respondData(
	res, await Create('neighborhood', { ...req.body })
)

/**
 * @api {GET} /neighborhood/ranking Get Ranking for neighborhoods
 * @apiName Ranking
 * @apiGroup Neighborhood
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {Array} data.ranking Ranking of neighborhoods
 * @apiSuccess {Number} data.ranking.fellows Fellows
 * @apiSuccess {Number} data.ranking.name Neighborhood name
 * @apiSuccess {Number} data.ranking.id Neighborhood ID
 * @apiSuccess {Number} data.ranking.areInfected Are infected
 *
 * @apiUse MetaDataResponse
 */
exports.ranking = async (req, res) => {
	req.queryOptions.addSearchTermColumns(['name'])
	const neighborhoods = await GetAll('neighborhood', req.queryOptions, {
		attributes: ['fellows', 'areInfected', 'name', 'id']
	})
	return respondData(res, neighborhoods)
}

/**
 * @api {GET} /neighborhood/admin-ranking Get Admin Ranking for neighborhoods
 * @apiName AdminRanking
 * @apiGroup Neighborhood
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {Array} data.ranking Ranking of neighborhoods
 * @apiSuccess {Number} data.ranking.fellows Fellows
 * @apiSuccess {Number} data.ranking.name Neighborhood name
 * @apiSuccess {Number} data.ranking.id Neighborhood ID
 * @apiSuccess {Number} data.ranking.areInfected Are infected
 * @apiSuccess {Number} data.ranking.areQuarantined Are Quarantined
 * @apiSuccess {Number} data.ranking.areOnRisk Are On Risk
 * @apiSuccess {Number} data.ranking.areExposed Are Exposed
 * @apiSuccess {Number} data.ranking.areWithSymptoms Are With Symptoms
 * @apiSuccess {Number} data.ranking.areHealthy Are Healthy
 *
 *
 *
 * @apiUse MetaDataResponse
 */
exports.adminRanking = async (req, res) => {
	req.queryOptions.addSearchTermColumns(['name'])
	const neighborhoods = await GetAll('neighborhood', req.queryOptions, {
		attributes: [
			'fellows',
			'areInfected',
			'name',
			'id',
			'areQuarantined',
			'areOnRisk',
			'areExposed',
			'areWithSymptoms',
			'areHealthy'
		]
	})
	return respondData(res, neighborhoods)
}
