'use strict'

const {
	GetAll,
	GetOneBy,
	Create,
	Update,
	Delete
} = require('services/generic.services')

const { respondData } = require('helpers/response.helper')

/**
 * @api {GET} /country Get countries
 * @apiName GetCountries
 * @apiGroup Country
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[[country]](#api-Modelos-ObjectCountry)} data.rows[] Array of countries
 * @apiUse MetaDataResponse
 */
exports.getAll = async (req, res) => {
	req.queryOptions.addSearchTermColumns(['name'])

	if (req.data.user.profile === "operator") {
		req.queryOptions.addCustomAnd(`"country"."id" IN (${req.data.cities.map((item) => item.idCountry)}) `)
	}
	const countries = await GetAll('country', req.queryOptions)
	return respondData(res, countries)
}

/**
 * @api {GET} /country/:id Get single country
 * @apiName GetCountry
 * @apiGroup Country
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Country ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[country](#api-Modelos-ObjectCountry)} data Country
 */
exports.getOneById = async (req, res) => respondData(
	res, await GetOneBy('country', { id: req.params.id }, req.queryOptions.scopes)
)

/**
 * @api {GET} /country/mobile Get countries for mobile
 * @apiName GetCountriesMobile
 * @apiGroup Country
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[[country]](#api-Modelos-ObjectCountry)} data.rows[] Array of countries
 * @apiUse MetaDataResponse
 */
exports.getAllMobile = async (req, res) => {
	req.queryOptions.addSearchTermColumns(['name'])
	const countries = await GetAll('country', req.queryOptions, {
		attributes: ['fellows', 'areInfected', 'name', 'id']
	})
	return respondData(res, countries)
}

/**
 * @api {GET} /country/mobile/:id Get single country for mobile
 * @apiName GetCountryMobile
 * @apiGroup Country
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Country ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[country](#api-Modelos-ObjectCountry)} data.country Country
 */
exports.getOneByIdMobile = async (req, res) => respondData(
	res, {
	country: await GetOneBy('country', { id: req.params.id }, req.queryOptions.scopes,
		{ attributes: ['fellows', 'areInfected', 'name', 'id'] }
	)
}
)

/**
 * @api {PUT} /country/:id Update country
 * @apiName UpdateCountry
 * @apiGroup Country
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Country ID
 * @apiUse CountryModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[country](#api-Modelos-ObjectCountry)} data Country
 */
exports.update = async (req, res) => respondData(
	res, await Update(
		'country',
		req.body,
		{ id: req.params.id }
	)
)

/**
 * @api {DELETE} /country/:id Delete country
 * @apiName DeleteCountry
 * @apiGroup Country
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Country ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[country](#api-Modelos-ObjectCountry)} data Country
 */
exports.deleteEntity = async (req, res) => respondData(
	res, await Delete(
		'country',
		{ id: req.params.id }
	)
)


/**
 * @api {POST} /country Create country
 * @apiName CreateCountry
 * @apiGroup Country
 * @apiVersion 1.0.0
 *
 * @apiUse CountryModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[country](#api-Modelos-ObjectCountry)} data Country
 */
exports.create = async (req, res) => respondData(
	res, await Create('country', { ...req.body })
)
