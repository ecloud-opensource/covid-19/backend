'use strict'

const {
  GetAll,
  GetOneBy,
  Create,
  Update,
  Delete
} = require('services/generic.services')

const { respondData } = require('helpers/response.helper')

/**
 * @api {GET} /state Get states
 * @apiName GetStates
 * @apiGroup State
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[state[]](#api-Modelos-ObjectState)} data.rows[] Array of state
 * @apiUse MetaDataResponse
 */
exports.getAll = async (req, res) => {
  req.queryOptions.addSearchTermColumns(['name'])
  if (req.data.user.profile === "operator") {
    req.queryOptions.addCustomAnd(`"state"."id" IN (${req.data.cities.map((item) => item.idState)}) `)
  }
  const states = await GetAll('state', req.queryOptions)
  return respondData(res, states)
}

/**
 * @api {GET} /state/:id Get single state
 * @apiName GetState
 * @apiGroup State
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id State ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[state](#api-Modelos-ObjectState)} data State
 */
exports.getOneById = async (req, res) => respondData(
  res, await GetOneBy('state', { id: req.params.id }, req.queryOptions.scopes)
)


/**
 * @api {GET} /state/mobile Get state for mobile
 * @apiName GetStates
 * @apiGroup State
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[state[]](#api-Modelos-ObjectState)} data.rows[] Array of state
 * @apiUse MetaDataResponse
 */
exports.getAllMobile = async (req, res) => {
  req.queryOptions.addSearchTermColumns(['name'])
  const states = await GetAll('state', req.queryOptions, {
    attributes: ['fellows', 'areInfected', 'name', 'id']
  })
  return respondData(res, states)
}

/**
 * @api {GET} /state/mobile/:id Get single state for mobile
 * @apiName GetStateMobile
 * @apiGroup State
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id State ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[state](#api-Modelos-ObjectState)} data.state State
 */
exports.getOneByIdMobile = async (req, res) => respondData(
  res, {
  state: await GetOneBy('state', { id: req.params.id }, req.queryOptions.scopes,
    { attributes: ['fellows', 'areInfected', 'name', 'id'] })
}
)

/**
 * @api {GET} /state/log Get state log for admin
 * @apiName GetStateLog
 * @apiGroup State
 * @apiVersion 1.0.0
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[stateLog](#api-Modelos-ObjectStateLog)} data.stateLog State log
 */
exports.getLog = async (req, res) => {
  const stateLog = await GetAll('stateLog', req.queryOptions)
  return respondData(res, stateLog)
}

/**
 * @api {PUT} /state/:id Update state
 * @apiName UpdateState
 * @apiGroup State
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id State ID
 * @apiUse StateModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[state](#api-Modelos-ObjectState)} data State
 */
exports.update = async (req, res) => respondData(
  res, await Update(
    'state',
    req.body,
    { id: req.params.id }
  )
)


/**
 * @api {DELETE} /state/:id Delete state
 * @apiName DeleteState
 * @apiGroup State
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id State ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[state](#api-Modelos-ObjectState)} data State
 */
exports.deleteEntity = async (req, res) => respondData(
  res, await Delete(
    'state',
    { id: req.params.id }
  )
)


/**
 * @api {POST} /state Create state
 * @apiName CreateState
 * @apiGroup State
 * @apiVersion 1.0.0
 *
 * @apiUse StateModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[state](#api-Modelos-ObjectState)} data State
 */
exports.create = async (req, res) => respondData(
  res, await Create('state', { ...req.body })
)

/**
 * @api {GET} /state/ranking Get Ranking for states
 * @apiName Ranking
 * @apiGroup State
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {Array} data.ranking Ranking of states
 * @apiSuccess {Number} data.ranking.fellows Fellows
 * @apiSuccess {Number} data.ranking.name State name
 * @apiSuccess {Number} data.ranking.id State ID
 * @apiSuccess {Number} data.ranking.areInfected Are infected
 *
 * @apiUse MetaDataResponse
 */
exports.ranking = async (req, res) => {
  req.queryOptions.addSearchTermColumns(['name'])
  const states = await GetAll('state', req.queryOptions, {
    attributes: ['fellows', 'areInfected', 'name', 'id']
  })
  return respondData(res, states)
}

/**
 * @api {GET} /state/admin-ranking Get Admin Ranking for states
 * @apiName AdminRanking
 * @apiGroup State
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {Array} data.ranking Ranking of states
 * @apiSuccess {Number} data.ranking.fellows Fellows
 * @apiSuccess {Number} data.ranking.name State name
 * @apiSuccess {Number} data.ranking.id State ID
 * @apiSuccess {Number} data.ranking.areInfected Are infected
 * @apiSuccess {Number} data.ranking.areQuarantined Are Quarantined
 * @apiSuccess {Number} data.ranking.areOnRisk Are On Risk
 * @apiSuccess {Number} data.ranking.areExposed Are Exposed
 * @apiSuccess {Number} data.ranking.areWithSymptoms Are With Symptoms
 * @apiSuccess {Number} data.ranking.areHealthy Are Healthy
 *
 *
 *
 * @apiUse MetaDataResponse
 */
exports.adminRanking = async (req, res) => {
  req.queryOptions.addSearchTermColumns(['name'])
  const states = await GetAll('state', req.queryOptions, {
    attributes: [
      'fellows',
      'areInfected',
      'name',
      'id',
      'areQuarantined',
      'areOnRisk',
      'areExposed',
      'areWithSymptoms',
      'areHealthy'
    ]
  })
  return respondData(res, states)
}
