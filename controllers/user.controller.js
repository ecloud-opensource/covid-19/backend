'use strict'

const {
  GetAll,
  GetOneBy,
  Create,
  Update,
  Delete,
  BulkCreate,
  Count
} = require('services/generic.services')

const {
  findPlace
} = require('services/geo.services')

const {
  addSecondsToDate,
  diffInSecondsBetweenTwoDates,
  getDatetime,
  secondsToHoursMinutesSeconds,
  isExpired
} = require("helpers/datetime.helper");

const {
  getToken
} = require("helpers/auth.helper");


const { randomIntCode } = require("helpers/numeric.helper")
const { respondData, respondBadRequest, respondMessage, respondError, respondOperationFailed } = require('helpers/response.helper')
const { sendSMSViaWhatsApp, sendSMSViaTwilio } = require("services/message.services")
const { sendContactEmail } = require("services/email.services")


/**
 * @api {POST} /user/phone-validation Validate user phone
 * @apiName PhoneValidation
 * @apiGroup Users
 * @apiVersion 1.0.0
 *
 * @apiParam {string} phone User's phone
 * @apiParam {string} identification User's identification
 *
 * @apiUse MessageResponse
 */
exports.phoneValidation = async (req, res) => {
  try {



    const options = {
      phone: req.body.phone
    };

    if (options.phone === "+11111100100" || options.phone === "+12015550123") {
      return respondMessage(res, global.__i18n.__("user.phoneValidation.sendCodeByWhatsApp"));
    }


    const pv = { code: randomIntCode(6), ...options };

    let phoneValidationEntity = await GetOneBy('phoneValidation', options, {}, {}, false);

    if (phoneValidationEntity) {

      const pinExpiration = await GetOneBy('setting', { code: "SMS_RESEND_TIME" });

      if (pinExpiration.active) {

        const diffLastSend = +diffInSecondsBetweenTwoDates(
          getDatetime(),
          addSecondsToDate(phoneValidationEntity.updatedAt, +pinExpiration.value));
        if (diffLastSend > 0) {
          return respondError(res, global.__i18n.__("user.phoneValidation.waitToResendSMS", secondsToHoursMinutesSeconds(diffLastSend)));
        }
      }

      await Update('phoneValidation', pv, { id: phoneValidationEntity.id });

    } else {
      phoneValidationEntity = await Create('phoneValidation', pv);
    }


    sendSMSViaWhatsApp(pv.phone.replace("+", ""), pv.code).then((msjResponse) => {
      return respondMessage(res, global.__i18n.__("user.phoneValidation.sendCodeByWhatsApp"));
    }).catch((err) => {
      sendSMSViaTwilio(pv.phone, pv.code).then((msjResponse) => {
        return respondMessage(res, global.__i18n.__("user.phoneValidation.sendCodeBySMS"));
      }).catch((err) => {
        return respondError(res, global.__i18n.__("defaults.ERROR"));
      });
    });

  } catch (e) {
    return respondOperationFailed(res, e);
  }
}


/**
* @api {POST} /user/code-validation Request code validation
* @apiName CodeValidation
* @apiGroup Users
* @apiVersion 1.0.0
*
* @apiParam {string} phone Phone
* @apiParam {string} code Code
* @apiParam {string} firebaseToken Firebase Token
*
* @apiUse SuccessResponse
*/

exports.codeValidation = async (req, res) => {
  try {

    const options = {
      phone: req.body.phone,
      code: req.body.code
    };

    const phoneValidationEntity = await GetOneBy('phoneValidation', options, {}, {}, false);

    if (phoneValidationEntity) {
      const pinExpiration = await GetOneBy('setting', { code: "PHONE_PIN_EXPIRATION" });

      if (pinExpiration.active && isExpired(phoneValidationEntity.get("updatedAt"), +pinExpiration.value)) {
        return respondError(res, global.__i18n.__("user.codeValidation.codeExpired"));
      }

      if (!phoneValidationEntity.validated) {
        Update(
          'phoneValidation',
          { validated: true },
          { id: phoneValidationEntity.id },
        )
      }

      let user;
      if (phoneValidationEntity.idUser) {

        user = await GetOneBy('user', { id: phoneValidationEntity.idUser }, req.queryOptions.scopes, {
          attributes: { exclude: ['password'] }
        },
          false
        )

        if (!user) {
          return respondError(res, global.__i18n.__("user.codeValidation.userInactive"));
        }

        Update(
          'user',
          { firebaseToken: req.body.firebaseToken },
          { id: phoneValidationEntity.idUser },
        )

      } else {

        user = await Create('user', {
          phone: phoneValidationEntity.phone,
          firebaseToken: req.body.firebaseToken
        });

        phoneValidationEntity.set('idUser', user.id);
        phoneValidationEntity.save();

      }

      user = user.get({ plain: true });

      user.type = "user";

      return respondData(res, { user, token: getToken(user) });

    } else {
      return respondError(res, global.__i18n.__("user.codeValidation.codeInvalid"));
    }
  } catch (e) {
    return respondOperationFailed(res, e);
  }
};

/**
* @api {POST} /user/contact Contact
* @apiName Contact
* @apiGroup Users
* @apiVersion 1.0.0
*
* @apiParam {string} firstName First Name
* @apiParam {string} lastName Last name
* @apiParam {string} email Email
* @apiParam {string} message Message
*
* @apiUse SuccessResponse
*/

exports.contact = async (req, res) => {
  try {
    return respondData(res, await sendContactEmail(req.body))
  } catch (e) {
    return respondOperationFailed(res, e)
  }
}

/**
 * @api {GET} /user/:id Get User by ID
 * @apiName GetById
 * @apiGroup Users
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id User ID
 * 
 * @apiSuccess  {[user](#api-Modelos-ObjectUser)} data.user User
 * @apiUse SuccessResponse
 */

exports.getById = async (req, res) => respondData(
  res, await GetOneBy('user', { id: req.params.id }, req.queryOptions.scopes)
)

/**
 * @api {GET} /user/me Get Current User
 * @apiName GetMe
 * @apiGroup Users
 * @apiVersion 1.0.0
 *
 * @apiSuccess  {[user](#api-Modelos-ObjectUser)} data.user User
 * @apiUse SuccessResponse
 */

exports.getMe = async (req, res) => respondData(
  res, await GetOneBy('user', { id: req.data.user.id }, req.queryOptions.scopes)
)

/**
 * @api {GET} /user/ Get users
 * @apiName GetUsers
 * @apiGroup Users
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[[user]](#api-Modelos-ObjectUser)} data.rows[] Array of users
 * @apiUse MetaDataResponse
 */
exports.getAll = async (req, res) => {
  req.queryOptions.addSearchTermColumns(['firstName', 'lastName', 'email'])
  const users = await GetAll('user', req.queryOptions);
  return respondData(res, users)
}

/**
* @api {PUT} /user/:id Update User by ID
* @apiName PutById
* @apiGroup Users
* @apiVersion 1.0.0

* @apiParam {Integer} id User ID
* @apiParam  {[user](#api-Modelos-ObjectUser)} user User
*
* @apiSuccess  {[user](#api-Modelos-ObjectUser)} data.user User
* @apiUse SuccessResponse
*/

exports.updateById = async (req, res) => {
  try {
    return respondData(res, {
      user: await updateUser(req.body, await GetOneBy('user', { id: req.params.id }, req.queryOptions.scopes))
    })
  } catch (e) {
    return respondOperationFailed(res, e);
  }
}

/**
* @api {PUT} /user/me Update Current User
* @apiName PutMe
* @apiGroup Users
* @apiVersion 1.0.0
*
* @apiParam  {[user](#api-Modelos-ObjectUser)} user User
*
* @apiSuccess  {[user](#api-Modelos-ObjectUser)} data.user User
* @apiUse SuccessResponse
*/

exports.updateMe = async (req, res) => {
  try {
    return respondData(res, {
      user: await updateUser(req.body, req.data.user)
    })
  } catch (e) {
    return respondOperationFailed(res, e);
  }
}

/**
* @api {PUT} /user/position Get User Positions
* @apiName Positions
* @apiGroup Users
* @apiVersion 1.0.0
*
* @apiUse QueryOptionsRequest
*
* @apiSuccess  {[position](#api-Modelos-ObjectPosition)} data.position Position
* @apiUse SuccessResponse
*/

exports.getPositions = async (req, res) => {
  try {
    return respondData(res, { position: await GetAll('position', req.queryOptions) })
  } catch (e) {
    return respondOperationFailed(res, e);
  }
}


/**
* @api {PUT} /user/questionnaire Do questionnaire
* @apiName Questionnaire
* @apiGroup Users
* @apiVersion 1.0.0
*
* @apiParam  {[questionnaire](#api-Modelos-ObjectQuestionnaire)[]} questionnaire Questionnaire
*
* @apiUse MessageResponse
*/

exports.doQuestionnaire = async (req, res) => {
  try {

    const questionnaire = req.body.questionnaire.map((item) => {
      return { answer: item.answer, question: item.question, idUser: req.data.user.id }
    });

    let updateUser = {}

    const promises = [];
    if (req.data.user.questionnaireAnswered) {

      questionnaire.map((item) => { promises.push(Update('questionnaire', item, { question: item.question })) })

      await Promise.all(promises);

    } else {

      await BulkCreate('questionnaire', questionnaire);

      updateUser = { ...updateUser, questionnaireAnswered: true }
    }

    Update('user', updateUser, { id: req.data.user.id })

    return respondMessage(res, global.__i18n.__("user.questionnaire.do"))
  } catch (e) {
    return respondOperationFailed(res, e);
  }
}

/**
* @api {GET} /user/questionnaire Get questionnaire
* @apiName Get Questionnaire
* @apiGroup Users
* @apiVersion 1.0.0
*
* @apiParam  {[questionnaire](#api-Modelos-ObjectQuestionnaire)[]} questionnaire Questionnaire
*
* @apiUse MessageResponse
*/

exports.getQuestionnaire = async (req, res) => {
  try {
    req.queryOptions.query.where = { idUser: req.data.user.id };
    return respondData(res, await GetAll('questionnaire', req.queryOptions));
  } catch (e) {
    return respondOperationFailed(res, e);
  }
}

/**
* @api {GET} /user/admin-questionnaire/:idUser Get questionnaire for Admin
* @apiName Get Questionnaire for Admin
* @apiGroup Users
* @apiVersion 1.0.0
*
* @apiParam  {[questionnaire](#api-Modelos-ObjectQuestionnaire)[]} questionnaire Questionnaire
*
* @apiUse MessageResponse
*/

exports.getQuestionnaireForAdmin = async (req, res) => {
  try {
    req.queryOptions.query.where = { idUser: req.params.idUser };
    return respondData(res, await GetAll('questionnaire', req.queryOptions));
  } catch (e) {
    return respondOperationFailed(res, e);
  }
}

/**
 * @api {DELETE} /user/:id Delete user
 * @apiName DeleteUser
 * @apiGroup Users
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id User ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[user](#api-Modelos-ObjectUser)} data User
 */
exports.deleteEntity = async (req, res) => respondData(
  res, await Delete('user', { id: req.params.id })
)

const updateUser = (body, user) => {
  return new Promise(async (resolve, reject) => {
    try {

      if (body.safehouse) {
        const { neighborhood, city, state, country } = await findPlace(body.safehouse);

        body.idCountry = country.id;

        body.idState = state.id;

        body.idCity = city.id;

        body.idNeighborhood = neighborhood ? neighborhood.id : null;

        if (!user.safehouse || (
          body.safehouse.coordinates[0] != user.safehouse.coordinates[0] ||
          body.safehouse.coordinates[1] != user.safehouse.coordinates[1])) {

          Create('position', {
            position: body.safehouse,
            idUser: user.id
          });
        }
      }
      return resolve(Update('user', body, { id: user.id }))
    } catch (e) {
      return reject(e)
    }
  })
}
