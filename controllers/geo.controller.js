'use strict'

const {
  Create,
  Count,
  GetOneBy
} = require('services/generic.services')


const {
  getUserMap,
  nearQuarentineUser,
  getAdminMap,
  nearUser,
  getInfected
} = require('services/geo.services')

const { respondData, respondOperationFailed } = require('helpers/response.helper')

const models = require("models")

/**
 * @api {POST} /geo/nearby Get Near By For Fellows
 * @apiName NearBy
 * @apiGroup Geo
 * @apiVersion 1.0.0
 *
 * @apiParam {[point](#api-Modelos-ObjectPoint)} topLeft Extremo superior derecho
 * @apiParam {[point](#api-Modelos-ObjectPoint)} topRight Extremo superior izquierdo
 * @apiParam {[point](#api-Modelos-ObjectPoint)} bottomLeft Extremo inferior derecho
 * @apiParam {[point](#api-Modelos-ObjectPoint)} bottomRight Extremo inferior izquiero
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[User](#api-Modelos-ObjectUser)[]} data.user User
 * @apiSuccess {[Point](#api-Modelos-ObjectPoint)} data.user.latestPoint Latest Point
 * @apiSuccess {[Notifications](#api-Modelos-ObjectNotification)[]} data.notifications
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "data": {
 *        "users": [
 *         {
 *           "isQuarantined": true
 *           "latestPoint": {
 *              "type": "Point",
 *              "coordinates": [-32.947677, -60.630696]
 *            }
 *         },
*          {
 *           "isQuarantined": false
 *           "latestPoint": {
 *              "type": "Point",
 *              "coordinates": [-32.945411, -60.631638]
 *            }
 *         },
 *       ],
 *       "notifications": [
 *         {
 *            "type": "HEADER",
 *            "title": "Hay 20 infectados en tu zona"
 *         },
 *         {
 *            "type": "FOOTER",
 *            "title": "Nos cuidamos entre todos!"
 *         },
 *       ]
 *      }
 *     }
 *   }
 *
 */

/**
* @api {OBJECT} Notification Notification
* @apiGroup Modelos
* @apiVersion 1.0.0
* @apiParam {String} title Title
* @apiParam {String} type Type
*
*/

exports.userNearBy = async (req, res) => {
  try {

    const { idNeighborhood, idCity, idState, idCountry } = req.data.user;

    const [usersMap, countUsers, infected] = await Promise.all([
      getUserMap(req.body, req.data.user),
      Count('user', { idCity: req.data.user.idCity }),
      getInfected({ idNeighborhood, idCity, idState, idCountry })
    ]);

    const notifications = [
      {
        type: `HEADER`,
        title: infected
      },
      {
        type: `FOOTER`,
        title: countUsers ? global.__i18n.__("geo.notifications.fellows", countUsers) : global.__i18n.__("geo.notifications.zerofellows")
      },
      {
        type: `FOOTER`,
        title: global.__i18n.__("geo.notifications.footer1")
      },
      {
        type: `FOOTER`,
        title: global.__i18n.__("geo.notifications.footer2")
      }
    ]

    return respondData(res, { users: usersMap, notifications })
  } catch (e) {
    return respondOperationFailed(res, e);
  }
};

/**
 * @api {POST} /geo/admin-nearby Get Admin Near By For Fellows
 * @apiName AdminNearBy
 * @apiGroup Geo
 * @apiVersion 1.0.0
 *
 * @apiParam {[point](#api-Modelos-ObjectPoint)} topLeft Extremo superior derecho
 * @apiParam {[point](#api-Modelos-ObjectPoint)} topRight Extremo superior izquierdo
 * @apiParam {[point](#api-Modelos-ObjectPoint)} bottomLeft Extremo inferior derecho
 * @apiParam {[point](#api-Modelos-ObjectPoint)} bottomRight Extremo inferior izquiero
 * @apiParam {String} [status] Status
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[User](#api-Modelos-ObjectUser)[]} data.user User
 * @apiSuccess {[Point](#api-Modelos-ObjectPoint)} data.user.latestPoint Latest Point
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "data": {
 *        "users": [
 *         {
 *           "isQuarantined": true,
 *           "id": 1,
 *           "latestPoint": {
 *              "type": "Point",
 *              "coordinates": [-32.947677, -60.630696]
 *            }
 *         },
 *         {
 *           "isQuarantined": false,
 *           "id": 2,
 *           "latestPoint": {
 *              "type": "Point",
 *              "coordinates": [-32.945411, -60.631638]
 *            }
 *         },
 *       ]
 *      }
 *     }
 *   }
 *
 */

exports.adminNearBy = async (req, res) => {
  try {
    return respondData(res, { users: await getAdminMap(req.body, req.body.status) })
  } catch (e) {
    return respondOperationFailed(res, e);
  }
};


/**
* @api {POST} /geo/new-point Register new point
* @apiName NewPoint
* @apiGroup Geo
* @apiVersion 1.0.0
*
* @apiParam {[point](#api-Modelos-ObjectPoint)} point Geolocation
*
* @apiUse SuccessResponse
* @apiSuccess {[Position](#api-Modelos-ObjectPosition)} data.point Position
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "success": true,
*       "data": {
*           "point": {
*               "position": {
*                  "type": "Point",
*                  "coordinates": [
*                     -34.6965016,
*                      -58.2874915
*                  ]
*                },
*           "idUser": 2,
*           "nearQuarantine": true
*    }
*
*/


exports.newPoint = async (req, res) => {
  try {


    return respondData(res, {
      point:
        await Create('position', {
          position: req.body.point,
          idUser: req.data.user.id,
          nearQuarantine: await nearQuarentineUser(req.body.point, req.data.user) ? true : false,
          match: await nearUser(req.body.point, req.data.user)
        }, { user: req.data.user, include: { model: models.positionMatch, as: 'match' } })
    })
  } catch (e) {
    return respondOperationFailed(res, e);
  }
};



