'use strict'

const {
	GetAll,
	GetOneBy,
	Create,
	Update,
	Delete
} = require('services/generic.services')

const {
	hashPassword,
	comparePassword,
	getToken,
	generatePasswordRecoveryToken,
	decodeBase64,
	decodeToken
} = require('helpers/auth.helper')

const { respondData, respondBadRequest, respondMessage, respondError } = require('helpers/response.helper')
const { sendForgotPassword } = require('services/email.services')
const models = require("models")

/**
 * @api {GET} /admins Get admins
 * @apiName GetAdmins
 * @apiGroup Admins
 * @apiVersion 1.0.0
 *
 * @apiUse QueryOptionsRequest
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[[admin]](#api-Modelos-ObjectAdmin)} data.rows[] Array of admins
 * @apiUse MetaDataResponse
 */
exports.getAll = async (req, res) => {
	req.queryOptions.addSearchTermColumns(['firstName', 'lastName', 'email'])
	const admins = await GetAll('admin', req.queryOptions,
		{ attributes: { exclude: ['password'] } });
	return respondData(res, admins)
}

/**
 * @api {GET} /admins/:id Get single admin
 * @apiName GetAdmin
 * @apiGroup Admins
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Admin ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[admin](#api-Modelos-ObjectAdmin)} data Admin
 */
exports.getOneById = async (req, res) => respondData(
	res, await GetOneBy('admin', { id: req.params.id }, req.queryOptions.scopes, {
		attributes: { exclude: ['password'] }
	})
)

/**
 * @api {PUT} /admins/:id Update admin
 * @apiName UpdateAdmin
 * @apiGroup Admins
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Admin ID
 * @apiUse AdminModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[admin](#api-Modelos-ObjectAdmin)} data Admin
 */
exports.update = async (req, res) => {

	const transaction = await models.sequelize.transaction();

	try {

		if (req.body.password) {
			req.body.password = hashPassword(req.body.password)
		}

		const admin = await Update(
			'admin',
			req.body,
			{ id: req.params.id },
			{ attributes: { exclude: ['password'] }, transaction }
		)

		if (req.body.cities) {
			admin.setCities(req.body.cities, transaction);
		}

		await transaction.commit();

		return respondData(res, admin)
	} catch (err) {
		await transaction.rollback();
		return respondError(res, err)
	}
}

/**
 * @api {DELETE} /admins/:id Delete admin
 * @apiName DeleteAdmin
 * @apiGroup Admins
 * @apiVersion 1.0.0
 *
 * @apiParam {Integer} id Admin ID
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[admin](#api-Modelos-ObjectAdmin)} data Admin
 */
exports.deleteEntity = async (req, res) => respondData(
	res, await Delete(
		'admin',
		{ id: req.params.id },
		{ attributes: { exclude: ['password'] } }
	)
)


/**
 * @api {POST} /admins Create admin
 * @apiName CreateAdmin
 * @apiGroup Admins
 * @apiVersion 1.0.0
 *
 * @apiUse AdminModel
 *
 * @apiUse SuccessResponse
 * @apiSuccess {[admin](#api-Modelos-ObjectAdmin)} data Admin
 */
exports.create = async (req, res) => {

	const transaction = await models.sequelize.transaction();

	try {

		const admin = await Create('admin', { ...req.body, password: hashPassword(req.body.password) }, { transaction })

		if (req.body.cities) {
			admin.setCities(req.body.cities, transaction);
		}

		await transaction.commit();

		return respondData(res, admin)

	} catch (err) {
		await transaction.rollback();
		return respondError(res, err)
	}
}

/**
 * @api {POST} /admins/login Login admin
 * @apiName LoginAdmin
 * @apiGroup Admins
 * @apiVersion 1.0.0
 *
 * @apiParam email E-Mail
 * @apiParam password Password
 *
 * @apiUse SuccessResponse
 * @apiSuccess {String} data.token Token
 * @apiSuccess {[admin](#api-Modelos-ObjectAdmin)} data.admin Admin
 */
exports.login = async (req, res) => {
	const { email, password } = req.body

	if (!email) {
		return respondBadRequest(res, global.__i18n.__("defaults.EMAIL_REQUIRED"))
	}

	if (!password) {
		return respondBadRequest(res, global.__i18n.__("defaults.PASSWORD_REQUIRED"))
	}
	let admin = await GetOneBy('admin', { email })

	if (!comparePassword(password, admin.password)) {
		return respondBadRequest(res, global.__i18n.__("defaults.LOGIN_INVALID_CREDENTIALS"))
	}

	admin = admin.get({ plain: true });

	delete admin.password;

	admin.type = "admin";

	const token = getToken(admin)

	return respondData(res, {
		token,
		admin
	})
}

/**
 * @api {PUT} /admins/forgot-password Forgot password
 * @apiName ForgotPasswordAdmin
 * @apiGroup Admins
 * @apiVersion 1.0.0
 *
 * @apiParam {String} email E-mail
 *
 * @apiUse SuccessResponse
 */
exports.forgotPassword = async (req, res) => {
	const { email } = req.body

	if (!email) {
		return respondBadRequest(res, global.__i18n.__("defaults.EMAIL_REQUIRED"))
	}

	const admin = await GetOneBy('admin', { email }, undefined, { attribues: { exclude: ['password'] } })

	__logger.info(`adminController->forgotPassword: Found admin ${email}`)

	const payloadData = {
		email: admin.email.toLowerCase()
	}

	const token = generatePasswordRecoveryToken(payloadData, 'admin')

	__logger.info('Token for forgot-password admin email', email, token)
	const resetLink = `${__config.clientUrl}/auth/reset-password/${token}`

	await sendForgotPassword({ name: admin.firstName, email: admin.email }, resetLink)

	return respondMessage(res, global.__i18n.__("defaults.FORGOT_PASSWORD_EMAIL_SENT"))
}

/**
 * @api {PUT} /admins/reset-password Reset password
 * @apiName ResetPasswordAdmin
 * @apiGroup Admins
 * @apiVersion 1.0.0
 *
 * @apiParam {String} token Password reset token
 * @apiParam {String} newPassword New password
 *
 * @apiUse SuccessResponse
 */
exports.resetPassword = async (req, res) => {
	const { token, newPassword } = req.body

	if (!token) {
		return respondBadRequest(res, global.__i18n.__("defaults.TOKEN_REQUIRED"))
	}

	if (!newPassword) {
		return respondBadRequest(res, global.__i18n.__("defaults.NEW_PASSWORD_REQUIRED"))
	}

	const base64 = decodeBase64(token)
	const payloadData = decodeToken(base64.token)

	const admin = await GetOneBy('admin', { email: payloadData.email }, undefined)

	__logger.info(`adminController->resetPassword: Found admin ${admin.email}`)

	await Update('admin', { password: hashPassword(newPassword) }, { id: admin.getId() })

	return respondMessage(res, global.__i18n.__("defaults.PASSWORD_SUCCESFULLY_CHANGED"))
}
