const cron = require('node-cron')
const models = require("models");
const statuses = require('catalogs/statuses')
const moment = require("moment");

module.exports = () => {
  cron.schedule('*/5 * * * *', () => {
    __logger.info(`cron-> 5 minutes`)
    this.updateFellows();
    this.updateStatus();
  });

  cron.schedule('0 * * * * ', () => {
    __logger.info(`cron-> Every hour`)
    this.updateQuarantined();
  });
}

exports.updateFellows = () => {
  __logger.info(`cron->Update Fellows`)

  models.user.count({ group: ['id_neighborhood'], attributes: ['idNeighborhood'] }).then(async (usersOnNeighborhoods) => {
    for (const neighborhood of usersOnNeighborhoods) {
      if (neighborhood.idNeighborhood) {
        await models.neighborhood.update({ fellows: neighborhood.count }, { where: { id: neighborhood.idNeighborhood } })
      }
    }
  });

  models.user.count({ group: ['id_city'], attributes: ['idCity'] }).then(async (usersOnCities) => {
    for (const city of usersOnCities) {
      if (city.idCity) {
        await models.city.update({ fellows: city.count }, { where: { id: city.idCity } })
      }
    }
  });

  models.user.count({ group: ['id_state'], attributes: ['idState'] }).then(async (usersOnStates) => {
    for (const state of usersOnStates) {
      if (state.idState) {
        await models.state.update({ fellows: state.count }, { where: { id: state.idState } })
      }
    }
  });

  models.user.count({ group: ['id_country'], attributes: ['idCountry'] }).then(async (usersOnCountries) => {
    for (const country of usersOnCountries) {
      if (country.idCountry) {
        await models.country.update({ fellows: country.count }, { where: { id: country.idCountry } })
      }
    }
  });
}

exports.updateQuarantined = () => {
  __logger.info(`cron->Update Quarantined`)
  models.user.update({ isQuarantined: false, finishQuarantine: true, status: statuses.CODE.HEALTHY }, { where: { isQuarantined: true, quarantineTo: { $lt: moment() } } })
}


exports.updateStatus = async () => {
  __logger.info(`cron->Update Status`)
  const transaction = await models.sequelize.transaction();

  try {

    const resetValues = {}
    for (const key of Object.keys(statuses.FIELD)) {
      resetValues[statuses.FIELD[key]] = 0;
    }

    let userStatusNeighborhood = await models.user.count({ group: ['status', 'id_neighborhood'], attributes: ['status', 'idNeighborhood'] })

    await models.neighborhood.update(resetValues, { transaction })

    for (const neighborhood of userStatusNeighborhood) {
      if (neighborhood.idNeighborhood) {
        let neighborhoodUpdate = {}
        neighborhoodUpdate[statuses.FIELD[neighborhood.status]] = neighborhood.count;
        await models.neighborhood.update(neighborhoodUpdate, { where: { id: neighborhood.idNeighborhood }, transaction })
      }
    }


    let userStatusCity = await models.user.count({ group: ['status', 'id_city'], attributes: ['status', 'idCity'] })

    await models.city.update(resetValues, { transaction })

    for (const city of userStatusCity) {
      if (city.idCity) {
        let cityUpdate = {}
        cityUpdate[statuses.FIELD[city.status]] = city.count;
        await models.city.update(cityUpdate, { where: { id: city.idCity }, transaction })
      }
    }


    let userStatusStates = await models.user.count({ group: ['status', 'id_state'], attributes: ['status', 'idState'] })

    await models.state.update(resetValues, { transaction })

    for (const state of userStatusStates) {
      if (state.idState) {
        let stateUpdate = {}
        stateUpdate[statuses.FIELD[state.status]] = state.count;
        await models.state.update(stateUpdate, { where: { id: state.idState }, transaction })
      }
    }

    let userStatusCountry = await models.user.count({ group: ['status', 'id_country'], attributes: ['status', 'idCountry'] });

    await models.country.update(resetValues, { transaction })

    for (const country of userStatusCountry) {
      if (country.idCountry) {
        let countryUpdate = {}
        countryUpdate[statuses.FIELD[country.status]] = country.count;
        await models.country.update(countryUpdate, { where: { id: country.idCountry }, transaction })
      }
    }

    await transaction.commit();

  } catch (err) {
    transaction.rollback();
  }


}