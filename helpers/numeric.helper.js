const randomIntFromInterval = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);

exports.randomIntFromInterval = randomIntFromInterval
exports.trimTwoDecimals = (num) => num ? Number.parseFloat(num.toFixed(2)) : num;

exports.randomIntCode = (numberOfDigits) => {
  let code = "";
  for (let i = 1; i <= numberOfDigits; i++) {
    code = code + randomIntFromInterval(0, 9);
  }
  return code;
};

exports.formatSMSCPhoneNumber = (phone) => {
  if (phone.substring(0, 4).indexOf("+549") !== -1) {
    return phone.slice(4);
  } else if (phone.substring(0, 3).indexOf("+54") !== -1) {
    return phone.slice(3);
  } else if (phone.substring(0, 3).indexOf("549") !== -1) {
    return phone.slice(3);
  } else if (phone.substring(0, 2).indexOf("54") !== -1) {
    return phone.slice(2);
  } else if (phone.substring(0, 1).indexOf("9") !== -1) {
    return phone.slice(1);
  } else {
    return phone;
  }
};