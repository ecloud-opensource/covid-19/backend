const jwt = require('jwt-simple')
const bcrypt = require('bcrypt')
const { getDatetime } = require('helpers/datetime.helper')

// BASE64
exports.encodeBase64 = (data) => Buffer.from(JSON.stringify(data)).toString('base64')
exports.decodeBase64 = (data) => JSON.parse(Buffer.from(data.toString(), 'base64').toString())

// Private JWT
exports.decodeToken = (token) => jwt.decode(token, __config.jwtSalt);

// PASSWORD HASH
exports.hashPassword = (password) => bcrypt.hashSync(password, 10)

exports.comparePassword = (password, hash) => bcrypt.compareSync(password, hash)
// TOKEN GENERATION
exports.getToken = (user) => exports.encodeBase64({
	token: jwt.encode(user, __config.jwtSalt),
	expiration: getDatetime().add(__config.expirations.token, 'seconds').unix()
})

// PASSWORD RECOVERY TOKEN
exports.generatePasswordRecoveryToken = (data, type) => exports.encodeBase64({
	token: jwt.encode({ id: data.id, email: data.email, type }, __config.jwtSalt),
	expiration: getDatetime().add(__config.expirations.passwordRecoveryEmail, 'seconds').unix()
})

// EMAIL VALIDATION TOKEN
exports.generateValidationEmailToken = (data, type) => exports.encodeBase64({
	token: jwt.encode({ id: data.id, email: data.email, type }, __config.jwtSalt),
	expiration: getDatetime().add(__config.auth.expirations.validationEmail, 'seconds').unix()
})
