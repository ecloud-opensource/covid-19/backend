const { output } = require('middleware/request-logger.middleware')
const moment = require('moment')
/**
 * @api {OBJECT} baseResponse baseResponse
 * @apiGroup Respuestas
 * @apiVersion 1.0.0
 * @apiHeader {Token} X-Api-Key Token de autorización
 * @apiParam {Boolean} status boolean
 * @apiParam {Object} data Data
 * @apiParam {String} message Message
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "data": {
 *        "users": [
 *         User1,
 *         User2
 *       ]
 *      }
 *     }
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 401 OK
 *     {
 *       "success": false,
 *       "message": "Unauthorized"
 *     }
 */

/**
 * @apiDefine SuccessResponse
 * @apiSuccess {Boolean} status true
 * @apiSuccess {Object} data Data
 */

/**
* @apiDefine MessageResponse
* @apiSuccess {Boolean} status true
* @apiSuccess {string} message Message
*/

/**
 * @apiDefine MetaDataResponse
 * @apiSuccess {Object} data.metadata Metadata
 * @apiSuccess {Object} data.metadata.totalResults Total results
 * @apiSuccess {Object} data.metadata.currentPage Current page
 * @apiSuccess {Object} data.metadata.pageSize Page size
 */

const respond = (res, response) => {
	output(res, response)
	res.send(response)
}

exports.respondData = (res, data) => {
	const response = {}
	response.status = true
	response.data = data
	res.status(200)

	respond(res, response)
}

exports.respondMessage = (res, message) => {
	const response = {}
	response.status = true
	response.message = message
	res.status(200)

	respond(res, response)
}

exports.respondOperationFailed = (res, error) => {
	const response = {}
	response.status = false
	response.message = global.__i18n.__("defaults.OPERATION_FAILED");
	res.status(500)
	__logger.error(`${moment()} --> Error: ${error}`)
	respond(res, response)
}

exports.respondBadRequest = (res, message) => {
	const response = {}
	response.status = false
	response.message = message || 'Bad request'
	res.status(400)

	respond(res, response)
}

exports.respondUnauthorized = (res, message) => {
	const response = {}
	response.status = false
	response.message = message || 'Unauthorized'
	res.status(401)

	respond(res, response)
}

exports.respondError = (res, message, error) => {
	const response = {}
	response.status = false
	response.message = message || 'Error'
	res.status(500)
	__logger.error(`${moment()} --> Error: ${error}`)
	respond(res, response)
}

exports.respondForbidden = (res) => {
	const response = {}
	response.status = false
	response.message = 'Forbidden'
	res.status(403)

	respond(res, response)
}

exports.respondNotFound = (res) => {
	const response = {}
	response.status = false
	response.message = 'Not Found'
	res.status(404)

	respond(res, response)
}


exports.notFound = (req, res) => {
	const response = {}
	response.status = false
	response.message = 'Url not found'
	res.status(404)

	respond(res, response)
}

// eslint-disable-next-line no-unused-vars
exports.error = (err, req, res, next) => {
	__logger.error(err)
	const response = {}
	response.status = false
	response.message = err.message
	res.status(err.status || 500)
	respond(res, response)
}
