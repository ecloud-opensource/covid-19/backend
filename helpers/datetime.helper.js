const moment = require('moment')

exports.getDatetime = (date) => (date ? moment(date) : moment())

exports.getUnixTimestamp = (date) => (date ? moment(date).unix() : moment().unix())

exports.getUnixTimestampInSeconds = () => moment().unix()

exports.getDatetimeWithFormat = (date, format) => moment(date, format)

exports.getStartOfDayWithFormat = (date, format) => moment(exports.getDatetime(date), format).startOf('day').format('YYYY-MM-DD HH:mm:ss')

exports.getEndOfDayWithFormat = (date, format) => moment(exports.getDatetime(date), format).endOf('day').format('YYYY-MM-DD HH:mm:ss')

exports.isExpired = (date, expiration) => exports.getDatetime() > exports.getDatetime(date).add(expiration, 'seconds')

exports.minutesFromNow = (date) => moment.duration(exports.getDatetime().diff(exports.getDatetimeWithFormat(date, 'DD/MM/YYYY HH:mm:ss'))).asMinutes()

exports.addMinutesToDate = (date, minutes) => exports.getDatetime(date).add(minutes, 'minutes')

exports.addSecondsToDate = (date, seconds) => exports.getDatetime(date).add(seconds, 'seconds')

exports.diffInHoursBetweenTwoDates = (startDate, endDate) => moment.duration(
	exports.getDatetime(endDate).diff(exports.getDatetime(startDate))
).asHours()

exports.diffInMinutesBetweenTwoDates = (startDate, endDate) => moment.duration(
	exports.getDatetime(endDate).diff(exports.getDatetime(startDate))
).asMinutes()

exports.diffInSecondsBetweenTwoDates = (startDate, endDate) => moment.duration(
	exports.getDatetime(endDate).diff(exports.getDatetime(startDate))
).asSeconds()

exports.substractMonthFromNow = (previousMonth) => moment().subtract(previousMonth, 'months').format('MMM YYYY')

exports.getFirstDayOfYear = (year) => moment().year(year).startOf('year').format('YYYY-MM-DD')

exports.getLastDayOfYear = (year) => moment().year(year).endOf('year').format('YYYY-MM-DD')

exports.getCurrentTimeLockFormat = () => moment().format('YYMMDDHHmmss')

exports.getCurrentTimeBBDDFormat = () => moment().format('YYYY-MM-DD HH:mm:ss')

exports.secondsToHoursMinutesSeconds = (a) => {
	const d = Number(a)
	const h = Math.floor(d / 3600)
	// eslint-disable-next-line no-mixed-operators
	const m = Math.floor(d % 3600 / 60)
	const s = Math.floor(d % 3600 % 60)

	const hDisplay = h > 0 ? h + (h === 1 ? ' hora ' : ' horas ') : ''
	const mDisplay = m > 0 ? m + (m === 1 ? ' minuto ' : ' minutos ') : ''
	const sDisplay = s > 0 ? s + (s === 1 ? ' segundo' : ' segundos') : ''
	let time = ''
	if (hDisplay) {
		time = hDisplay
	}
	if (mDisplay) {
		if (hDisplay) {
			time += `, ${mDisplay}`
		} else {
			time += mDisplay
		}
	}
	if (sDisplay) {
		if (mDisplay) {
			time += `y ${sDisplay}`
		} else {
			time += sDisplay
		}
	}
	return time
}
