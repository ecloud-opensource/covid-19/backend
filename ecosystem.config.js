module.exports = {
  apps: [
    {
      name: "COBEAT",
      script: "./bin/www",
      instances: -1,
      exec_mode: "cluster",
      env_staging: {
        "NODE_ENV": "staging",
        "PWD": "/var/www/backend"
      },
      env_production: {
        "NODE_ENV": "production",
        "PWD": "/var/www/backend"
      }
    }
  ]
}