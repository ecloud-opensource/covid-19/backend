'use strict'

const baseModel = require('models/base_model/base.model')

/**
 * @apiDefine NeighborhoodLogModel
 * @apiParam {String} oldValue Valor anterior del campo
 * @apiParam {String} newValue Valor actual del campo
 * @apiParam {String} field Nombre del campo que se modifico
 * @apiParam {Integer} idNeighborhood Barrio que se modifico.
 * @apiParam {[neighborhood](#api-Modelos-ObjectNeighborhood)} [neighborhood] Neighborhood
 * @apiParam {Integer} idAdmin Admin que lo modifico.
 * @apiParam {[admin](#api-Modelos-ObjectAdmin)} [admin] Admin
*/

/**
 * @apiDefine NeighborhoodLogRelations
 * @apiParam {[neighborhood](#api-Modelos-ObjectNeighborhood)} neighborhood Neighborhood
 */

/**
 * @api {OBJECT} neighborhoodLog Neighborhood Log
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse NeighborhoodLogModel
 * @apiUse NeighborhoodLogRelations
 */
module.exports = (sequelize, DataTypes) => {
	const entity = sequelize.define('neighborhoodLog', {

		...baseModel,

		oldValue: {
			type: DataTypes.TEXT,
			field: "old_value"
		},

		newValue: {
			type: DataTypes.TEXT,
			field: "new_value"
		},

		field: {
			type: DataTypes.STRING
		},


	}, {
		tableName: 'neighborhoodLog'
	})

	entity.associate = (models) => {
		entity.belongsTo(models.neighborhood, { foreignKey: { field: 'id_neighborhood', name: 'idNeighborhood' } })
		entity.belongsTo(models.admin, { foreignKey: { field: 'id_admin', name: 'idAdmin' } })
	}

	return entity
}
