'use strict'

const baseModel = require('models/base_model/base.model')
const logHook = require('models/base_model/logHook')

/**
 * @apiDefine CountryModel
 * @apiParam {String} name Name
 * @apiParam {String} code code
 * @apiParam {String} termsAndConditions Term and conditions url
 * @apiParam {Number} areInfected Are infected
 * @apiParam {Number} areQuarantined Are Quarantined
 * @apiParam {Number} areOnRisk Are On Risk
 * @apiParam {Number} areExposed Are Exposed
 * @apiParam {Number} areWithSymptoms Are With Symptoms
 * @apiParam {Number} areHealthy Are Healthy
 * @apiParam {Number} fellows Fellows
 * @apiParam {[point](#api-Modelos-ObjectPoint)} center Center
 * @apiParam {[polygon](#api-Modelos-ObjectPolygon)} area Area
 */

/**
 * @apiDefine CountryRelations
 * @apiParam {[city[]](#api-Modelos-ObjectCity)} cities Cities
 */

/**
 * @api {OBJECT} country Country
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse CountryModel
 * @apiUse CountryRelations
 * 
 * @apiDescription Available Scopes
 * - cities: Cities
 * - states: States
 */
module.exports = (sequelize, DataTypes) => {
	const entity = sequelize.define('country', {

		...baseModel,

		name: {
			type: DataTypes.STRING
		},

		code: {
			type: DataTypes.STRING
		},

		center: {
			type: DataTypes.GEOMETRY('POINT')
		},

		area: {
			type: DataTypes.GEOMETRY('POLYGON')
		},


		areInfected: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_infected'
		},

		areQuarantined: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_quarantined'
		},

		areOnRisk: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_on_risk'
		},

		areExposed: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_exposed'
		},

		areWithSymptoms: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_with_symptoms'
		},

		areHealthy: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_healthy'
		},

		fellows: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		},


		termsAndConditions: {
			type: DataTypes.STRING,
			field: 'terms_and_conditions'
		},


	}, {
		tableName: 'country'
	})

	entity.associate = (models) => {
		entity.hasMany(models.city, { foreignKey: { field: 'id_country', name: 'idCountry' }, as: "cities" })
		entity.hasMany(models.link, { foreignKey: { field: 'id_country', name: 'idCountry' }, as: 'link' })
	}

	entity.loadScopes = (models) => {
		entity.addScope('cities', {
			include: [
				{
					model: models.city,
					required: false,
					as: 'cities'
				}
			]
		})

		entity.addScope('states', {
			include: [
				{
					model: models.state,
					required: false,
					as: 'states'
				}
			]
		})
	}

	entity.loadHooks = (models) => {
		logHook(entity, models.countryLog, "idCountry")
	}

	return entity
}
