'use strict'

const baseModel = require('models/base_model/base.model')
const logHook = require('models/base_model/logHook')

/**
 * @apiDefine NeighborhoodModel
 * @apiParam {String} name Name
 * @apiParam {String} code Code
 * @apiParam {Number} areInfected Are infected
 * @apiParam {Number} areQuarantined Are Quarantined
 * @apiParam {Number} areOnRisk Are On Risk
 * @apiParam {Number} areExposed Are Exposed
 * @apiParam {Number} areWithSymptoms Are With Symptoms
 * @apiParam {Number} areHealthy Are Healthy
 * @apiParam {Number} fellows Fellows
 * @apiParam {[point](#api-Modelos-ObjectPoint)} center Center
 * @apiParam {[polygon](#api-Modelos-ObjectPolygon)} area Area
 */

/**
 * @apiDefine NeighborhoodRelations
 * @apiParam {[zone[]](#api-Modelos-ObjectZone)} zones Zones
 * @apiParam {[city](#api-Modelos-ObjectCity)} city City
 * @apiParam {[country](#api-Modelos-ObjectCountry)} country Country
 */

/**
 * @api {OBJECT} neighborhood Neighborhood
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse NeighborhoodModel
 * @apiUse NeighborhoodRelations
 * 
 * @apiDescription Available Scopes
 * - city: City
 * - country: Country
 */
module.exports = (sequelize, DataTypes) => {
	const entity = sequelize.define('neighborhood', {

		...baseModel,

		name: {
			type: DataTypes.STRING
		},

		code: {
			type: DataTypes.STRING
		},


		areInfected: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_infected'
		},

		areQuarantined: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_quarantined'
		},

		areOnRisk: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_on_risk'
		},

		areExposed: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_exposed'
		},

		areWithSymptoms: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_with_symptoms'
		},

		areHealthy: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_healthy'
		},

		fellows: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		},

		center: {
			type: DataTypes.GEOMETRY('POINT')
		},

		area: {
			type: DataTypes.GEOMETRY('POLYGON')
		}


	}, {
		tableName: 'neighborhood'
	})

	entity.associate = (models) => {

		entity.hasMany(models.neighborhoodLog, { foreignKey: { field: 'id_neighborhood', name: 'idNeighborhood' } })
		entity.belongsTo(models.city, { foreignKey: { field: 'id_city', name: 'idCity' }, as: 'city' })
		entity.belongsTo(models.country, { foreignKey: { field: 'id_country', name: 'idCountry' }, as: 'country' })
		entity.hasMany(models.zone, { foreignKey: { field: 'id_neighborhood', name: 'idNeighborhood' }, as: 'zones' })
	}

	entity.loadScopes = (models) => {
		entity.addScope('city', {
			include: [
				{
					model: models.city,
					required: true,
					as: 'city'
				}
			]
		})

		entity.addScope('country', {
			include: [
				{
					model: models.country,
					required: true,
					as: 'country'
				}
			]
		})

		entity.addScope('zones', {
			include: [
				{
					model: models.zone,
					required: false
				}
			]
		})
	}

	entity.loadHooks = (models) => {
		logHook(entity, models.neighborhoodLog, "idNeighborhood")
	}

	return entity
}
