'use strict'

const baseModel = require('models/base_model/base.model')

/**
 * @apiDefine AdminModel
 * @apiParam {String} firstName First name
 * @apiParam {String} lastName Last name
 * @apiParam {String} phone Phone
 * @apiParam {String} identification Identification
 * @apiParam {String} profession Profession
 * @apiParam {String} workplace Workplace
 * @apiParam {String} email E-Mail
 * @apiParam {String} password Password
 * @apiParam {String} avatarUrl Avatar URL
 * @apiParam {String="super_admin","operator"} profile Profile
 * @apiParam {Array} cities Array of Cities ID's
 *
 */

/**
 * @apiDefine AdminRelations
 * @apiParam {[city](#api-Modelos-ObjectCity)} cities Cities
 */

/**
 * @api {OBJECT} admin Admin
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse AdminModel
 * @apiUse AdminRelations
 */
module.exports = (sequelize, DataTypes) => {
	const entity = sequelize.define('admin', {

		...baseModel,

		firstName: {
			type: DataTypes.STRING,
			field: 'first_name'
		},

		lastName: {
			type: DataTypes.STRING,
			field: 'last_name'
		},

		email: {
			type: DataTypes.STRING
		},

		identification: {
			type: DataTypes.STRING
		},

		phone: {
			type: DataTypes.STRING
		},

		profession: {
			type: DataTypes.STRING
		},

		workplace: {
			type: DataTypes.STRING
		},

		password: {
			type: DataTypes.STRING
		},

		avatarUrl: {
			type: DataTypes.STRING,
			field: 'avatar_url'
		},

		profile: {
			type: DataTypes.STRING,
		},


	}, {
		tableName: 'admin'
	})

	entity.associate = (models) => {
		entity.belongsToMany(models.city, {
			foreignKey: 'idAdmin',
			otherKey: 'idCity',
			through: 'admin_city', as: 'cities'
		})

	}

	entity.loadScopes = (models) => {
		entity.addScope('cities', {
			include: [
				{
					model: models.city,
					required: false
				}
			]
		})
	}

	return entity
}
