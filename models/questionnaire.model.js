'use strict'

const baseModel = require('models/base_model/base.model')

/**
 * @apiDefine QuestionnaireModel
 * @apiParam {String} answer Answer
 * @apiParam {String} question Question
 *
 */

/**
 * @apiDefine QuestionnaireRelations
 * @apiParam {[user[]](#api-Modelos-ObjectUser)} user User
 */

/**
 * @api {OBJECT} Questionnaire Questionnaire
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse QuestionnaireModel
 * @apiUse QuestionnaireRelations
 */
module.exports = (sequelize, DataTypes) => {
  const entity = sequelize.define('questionnaire', {

    ...baseModel,

    answer: {
      type: DataTypes.TEXT
    },

    question: {
      type: DataTypes.STRING
    },

  }, {
    tableName: 'questionnaire'
  })

  entity.associate = (models) => {
    entity.belongsTo(models.user, { foreignKey: { field: 'id_user', name: 'idUser' }, constraints: false })
  }

  return entity
}
