'use strict'

const baseModel = require('models/base_model/base.model')

/**
 * @apiDefine LinkModel
 * @apiParam {String} url URL
 * @apiParam {String} title Title
 * @apiParam {String} imageUrl imageUrl
 * @apiParam {String} description Description
 * @apiParam {Number} order Order
 */

/**
 * @apiDefine LinkRelations
 * @apiParam {[city](#api-Modelos-ObjectCity)} city City
 * @apiParam {[country](#api-Modelos-ObjectCountry)} country Country
 *
 */

/**
 * @api {OBJECT} link Link
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse LinkModel
 * @apiUse LinkRelations
 * 
 * @apiDescription Available Scopes
 * - city: City
 * - country: Country
 */
module.exports = (sequelize, DataTypes) => {
	const entity = sequelize.define('link', {

		...baseModel,

		title: {
			type: DataTypes.STRING
		},

		url: {
			type: DataTypes.STRING
		},

		image: {
			type: DataTypes.STRING
		},

		description: {
			type: DataTypes.STRING
		},

		order: {
			type: DataTypes.INTEGER
		}


	}, {
		tableName: 'link'
	})

	entity.associate = (models) => {
		entity.belongsTo(models.city, { foreignKey: { field: 'id_city', name: 'idCity' }, as: 'city' })
		entity.belongsTo(models.country, { foreignKey: { field: 'id_country', name: 'idCountry' }, as: 'country' })
	}

	entity.loadScopes = (models) => {
		entity.addScope('city', {
			include: [
				{
					model: models.city,
					required: false,
					as: 'city'
				}
			]
		})

		entity.addScope('country', {
			include: [
				{
					model: models.country,
					required: false,
					as: 'country'
				}
			]
		})
	}

	return entity
}
