'use strict'

const baseModel = require('models/base_model/base.model')

/**
 * @apiDefine CountryLogModel
 * @apiParam {String} oldValue Valor anterior del campo
 * @apiParam {String} newValue Valor actual del campo
 * @apiParam {String} field Nombre del campo que se modifico
 * @apiParam {Integer} idCountry Ciudad que se modifico.
 * @apiParam {[country](#api-Modelos-ObjectCountry)} [country] Country
 * @apiParam {Integer} idAdmin Admin que lo modifico.
 * @apiParam {[admin](#api-Modelos-ObjectAdmin)} [admin] Admin
*/

/**
 * @apiDefine CountryLogRelations
 * @apiParam {[country](#api-Modelos-ObjectCountry)} country Country
 */

/**
 * @api {OBJECT} countryLog Country Log
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse CountryLogModel
 * @apiUse CountryLogRelations
 */
module.exports = (sequelize, DataTypes) => {
	const entity = sequelize.define('countryLog', {

		...baseModel,

		oldValue: {
			type: DataTypes.TEXT,
			field: "old_value"
		},

		newValue: {
			type: DataTypes.TEXT,
			field: "new_value"
		},

		field: {
			type: DataTypes.STRING
		},


	}, {
		tableName: 'countryLog'
	})

	entity.associate = (models) => {
		entity.belongsTo(models.country, { foreignKey: { field: 'id_country', name: 'idCountry' } })
		entity.belongsTo(models.admin, { foreignKey: { field: 'id_admin', name: 'idAdmin' } })
	}

	return entity
}