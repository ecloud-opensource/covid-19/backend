'use strict'

const baseModel = require('models/base_model/base.model')

/**
 * @apiDefine ZoneModel
 * @apiParam {String} name Name
 * @apiParam {[point](#api-Modelos-ObjectPoint)} center Center
 * @apiParam {[polygon](#api-Modelos-ObjectPolygon)} area Area
 *
 */

/**
 * @apiDefine ZoneRelations
 * @apiParam {[neighborhood](#api-Modelos-ObjectNeighborhood)} neighborhood Neighborhood
 */

/**
 * @api {OBJECT} zone Zone
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse ZoneModel
 * @apiUse ZoneRelations
 */
module.exports = (sequelize, DataTypes) => {
	const entity = sequelize.define('zone', {

		...baseModel,

		name: {
			type: DataTypes.STRING
		},

		code: {
			type: DataTypes.STRING
		},

		center: {
			type: DataTypes.GEOMETRY('POINT')
		},

		area: {
			type: DataTypes.GEOMETRY('POLYGON')
		}

	}, {
		tableName: 'zone'
	})

	entity.associate = (models) => {
		entity.belongsTo(models.neighborhood, { foreignKey: { field: 'id_neighborhood', name: 'idNeighborhood' } })
	}

	entity.loadScopes = (models) => {
		entity.addScope('neighborhood', {
			include: [
				{
					model: models.neighborhood,
					required: false
				}
			]
		})
	}

	return entity
}
