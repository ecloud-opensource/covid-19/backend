'use strict'

const baseModel = require('models/base_model/base.model')
const moment = require("moment")
const statuses = require("../catalogs/statuses")

/**
 * @apiDefine UserModel
 * @apiParam {String} firstName firstName
 * @apiParam {String} lastName lastName
 * @apiParam {String} phone Phone
 * @apiParam {String} email Email
 * @apiParam {String} address Address
 * @apiParam {String} number Number
 * @apiParam {[point](#api-Modelos-ObjectPoint)} safehouse Safehouse
 * @apiParam {Boolean} isQuarantined When the person is quarantined
 * @apiParam {Boolean} finishQuarantine When the person finish his quarantine
 * @apiParam {Date} quarantineFrom Quarantine from
 * @apiParam {Date} quarantineTo Quarantine to
 * @apiParam {Date} birthdate Birthdate
 * @apiParam {String} identification Identification
 * @apiParam {String="M","F","SD"} gender Gender
 * @apiParam {Boolean} pregnant Pregnant
 * @apiParam {Float} probability Probability
 * @apiParam {Boolean} outOfSafehouse Out of Safehouse
 * @apiParam {Boolean} onMovement On Movement
 * @apiParam {String} status Status
 * @apiParam {String} firebaseToken Firebase Token
 *
 */

/**
 * @apiDefine UserRelations
 * @apiParam {[country](#api-Modelos-ObjectCountry)} Country Country
 * @apiParam {[position](#api-Modelos-ObjectPosition)} latestPoint Latest Point
 * @apiParam {[neighborhood](#api-Modelos-ObjectNeighborhood)} Neighborhood Neighborhood
 * @apiParam {[city](#api-Modelos-ObjectCity)} City City
 * @apiParam {[state](#api-Modelos-ObjectState)} State State
 * @apiParam {[zone](#api-Modelos-ObjectZone)} Zone Zone
 */

/**
 * @api {OBJECT} User User
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse UserModel
 * @apiUse UserRelations
 * 
 * @apiDescription Available Scopes
 * - city: City
 * - country: Country
 * - neighborhood: neighborhood
 * - latestPosition: Latest Position
 * - positionsWithMatch: position with match
 * - positions: positions
 */


module.exports = (sequelize, DataTypes) => {
  const entity = sequelize.define('user', {

    ...baseModel,

    firstName: {
      type: DataTypes.STRING,
      field: 'first_name'
    },

    lastName: {
      type: DataTypes.STRING,
      field: 'last_name'
    },

    email: {
      type: DataTypes.STRING
    },

    identification: {
      type: DataTypes.STRING
    },

    phone: {
      type: DataTypes.STRING
    },

    address: {
      type: DataTypes.STRING
    },

    number: {
      type: DataTypes.STRING
    },

    safehouse: { type: DataTypes.GEOMETRY('POINT') },

    questionnaireAnswered: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      field: 'questionnaire_answered'
    },


    isQuarantined: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      field: 'is_quarantined'
    },

    finishQuarantine: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      field: 'finish_quarantine'
    },

    quarantineFrom: {
      type: DataTypes.DATE,
      field: 'quarantine_from'
    },

    quarantineTo: {
      type: DataTypes.DATE,
      field: 'quarantine_to'
    },

    birthdate: {
      type: DataTypes.DATE
    },

    gender: {
      type: DataTypes.STRING,
      defaultValue: "NE"
    },

    avatarUrl: {
      type: DataTypes.STRING,
      field: 'avatar_url'
    },

    pregnant: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },

    onMovement: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      field: 'on_movement'
    },

    outOfSafehouse: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      field: 'out_of_safehouse'
    },

    probability: {
      type: DataTypes.FLOAT
    },

    status: {
      type: DataTypes.STRING,
      defaultValue: statuses.CODE.HEALTHY
    },

    firebaseToken: {
      type: DataTypes.STRING,
      field: 'firebase_token'
    },


  }, {
    indexes: [
      { unique: true, fields: ['identification', 'id_country', 'is_deleted'], name: "indx_user_identification" },
      { unique: true, fields: ['phone', 'is_deleted'], name: "indx_user_phone" },
    ]
  }, {
    tableName: 'user'
  })

  entity.associate = (models) => {
    entity.belongsTo(models.state, { foreignKey: { field: 'id_state', name: 'idState' } })
    entity.belongsTo(models.city, { foreignKey: { field: 'id_city', name: 'idCity' } })
    entity.belongsTo(models.neighborhood, { foreignKey: { field: 'id_neighborhood', name: 'idNeighborhood' } })
    entity.belongsTo(models.country, { foreignKey: { field: 'id_country', name: 'idCountry' } })
    entity.belongsTo(models.zone, { foreignKey: { field: 'id_zone', name: 'idZone' } })
    entity.hasMany(models.position, { foreignKey: { field: 'id_user', name: 'idUser' }, as: 'positions' })
    entity.belongsTo(models.position, { foreignKey: { field: 'id_latest_position', name: 'idLatestPosition' }, constraints: false, as: 'latestPosition' })
  }


  entity.loadScopes = (models) => {
    entity.addScope('city', {
      include: [
        {
          model: models.city,
          required: false,
          as: 'city'
        }
      ]
    })

    entity.addScope('country', {
      include: [
        {
          model: models.country,
          required: false,
          as: 'country'
        }
      ]
    })

    entity.addScope('neighborhood', {
      include: [
        {
          model: models.neighborhood,
          required: false
        }
      ]
    })


    entity.addScope('latestPosition', {
      include: [
        {
          model: models.position,
          required: false,
          as: 'latestPosition'
        }
      ]
    })

    entity.addScope('positionsWithMatch', {
      include: [
        {
          model: models.position,
          required: false,
          as: 'positions',
          include: [
            {
              model: models.positionMatch,
              required: false,
              as: 'match',
              include: [
                {
                  model: models.user,
                  required: true,
                  as: 'user'
                }
              ]
            }
          ]
        }
      ]
    })

    entity.addScope('positions', {
      include: [
        {
          model: models.position,
          required: false,
          as: 'positions'
        }
      ]
    })
  }


  entity.loadHooks = (models) => {

    entity.addHook('beforeBulkUpdate', async (instance, options) => {

      let instanceAttributes = instance.attributes;
      let instanceFields = instance.fields;

      models.user.findOne({ where: { ...instance.where }, plain: true }).then((user) => {
        if (user) {

          if (instanceFields.indexOf("outOfSafehouse") != -1) {
            models.misbehavior.findOne({ where: { idUser: user.id, to: { $eq: null } }, order: [['id', 'DESC']], limit: 1 }).then((misbehavior) => {
              if (misbehavior) {
                if (!instanceAttributes.outOfSafehouse) {
                  misbehavior.set('to', moment());
                  misbehavior.save();
                }
              } else if (instanceAttributes.outOfSafehouse) {
                models.misbehavior.create({ idUser: user.id, idCity: user.idCity, idCountry: user.idCountry, idNeighborhood: user.idNeighborhood })
              }
            })
          }

          if (!user.isQuarantined && instanceFields.indexOf("isQuarantined") != -1 && instanceAttributes.isQuarantined) {
            models.setting.findOne({ where: { code: 'QUARANTINE_DAYS' } }).then((setting) => {
              user.quarantineFrom = moment();
              user.quarantineTo = moment().add(+setting.value, 'days');
              user.status = statuses.CODE.QUARANTINED;
              user.save();
            })
          } else if (user.isQuarantined && instanceFields.indexOf("isQuarantined") != -1 && !instanceAttributes.isQuarantined) {
            user.quarantineTo = moment();
            user.status = statuses.CODE.HEALTHY;
            user.finishQuarantine = true;
            user.save();
          }
        }
      })
    })
  }


  return entity

}