'use strict'

const baseModel = require('models/base_model/base.model')

/**
 * @apiDefine CityLogModel
 * @apiParam {String} oldValue Valor anterior del campo
 * @apiParam {String} newValue Valor actual del campo
 * @apiParam {String} field Nombre del campo que se modifico
 * @apiParam {Integer} idCity Barrio que se modifico.
 * @apiParam {[city](#api-Modelos-ObjectCity)} [city] City
 * @apiParam {Integer} idAdmin Admin que lo modifico.
 * @apiParam {[admin](#api-Modelos-ObjectAdmin)} [admin] Admin
*/

/**
 * @apiDefine CityLogRelations
 * @apiParam {[city](#api-Modelos-ObjectCity)} city City
 */

/**
 * @api {OBJECT} cityLog City Log
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse CityLogModel
 * @apiUse CityLogRelations
 */
module.exports = (sequelize, DataTypes) => {
	const entity = sequelize.define('cityLog', {

		...baseModel,

		oldValue: {
			type: DataTypes.TEXT,
			field: "old_value"
		},

		newValue: {
			type: DataTypes.TEXT,
			field: "new_value"
		},

		field: {
			type: DataTypes.STRING
		},


	}, {
		tableName: 'cityLog'
	})

	entity.associate = (models) => {
		entity.belongsTo(models.city, { foreignKey: { field: 'id_city', name: 'idCity' } })
		entity.belongsTo(models.admin, { foreignKey: { field: 'id_admin', name: 'idAdmin' } })
	}

	return entity
}
