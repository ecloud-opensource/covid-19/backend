const { isEqual } = require("../../helpers/validation.helper")

module.exports = (entity, entityLog, idField) => {
  entity.addHook('afterCreate', async (instance, options) => {
    if (options && options.customData && options.customData.currentAdmin) {
      let query = {};
      if (options.transaction) {
        query.transaction = options.transaction;
      }
      for (const field in instance['dataValues']) {
        if (!['createdAt', 'updatedAt', 'id', 'password'].includes(field)) {

          if (instance['dataValues'][field]) {

            if (typeof instance['dataValues'][field] == 'object' && instance['dataValues'][field]['type']) {
              switch (instance['dataValues'][field]['type']) {
                case "Polygon":
                  instance['dataValues'][field] = instance['dataValues'][field]['coordinates'][0].join(";")
                  break;
                case "Point":
                  instance['dataValues'][field] = instance['dataValues'][field]['coordinates'].join()
                  break;
              }
            }

            entityLog.create({
              newValue: instance['dataValues'][field] !== null ? instance['dataValues'][field].toString() : null,
              field: field,
              [idField]: instance.id,
              idAdmin: options.customData.currentAdmin.id
            }, query)
          }
        }
      }
    }
  })

  entity.addHook('beforeBulkUpdate', async (instance, options) => {
    if (instance.customData && instance.customData.currentAdmin) {

      let query = {};
      if (instance && instance.transaction) {
        query.transaction = instance.transaction;
      }
      const previousInstance = await entity.findOne({ where: { id: instance.where.id } });
      for (const field in instance['attributes']) {
        if (!['createdAt', 'updatedAt', 'id', 'password'].includes(field)) {
          if (previousInstance && !isEqual(instance['attributes'][field], previousInstance['dataValues'][field])) {
            let newValue = instance['attributes'][field] !== null ? instance['attributes'][field].toString() : null;
            let oldValue = previousInstance['dataValues'][field] !== null ? previousInstance['dataValues'][field].toString() : null;
            if (instance['attributes'][field] && typeof instance['attributes'][field] == 'object' && instance['attributes'][field]['type']) {
              switch (instance['attributes'][field]['type']) {
                case "Polygon":
                  newValue = instance['attributes'][field]['coordinates'][0].join(";")
                  break;
                case "Point":
                  newValue = instance['attributes'][field]['coordinates'].join()
                  break;

              }
            }
            if (previousInstance['dataValues'][field] && typeof previousInstance['dataValues'][field] == 'object' && previousInstance['dataValues'][field]['type']) {
              switch (previousInstance['dataValues'][field]['type']) {
                case "Polygon":
                  oldValue = previousInstance['dataValues'][field]['coordinates'][0].join(";")
                  break;
                case "Point":
                  oldValue = previousInstance['dataValues'][field]['coordinates'].join()
                  break;
              }
            }
            entityLog.create({
              newValue,
              oldValue,
              field: field,
              [idField]: instance.where.id,
              idAdmin: instance.customData ? (instance.customData.currentAdmin ? instance.customData.currentAdmin.id : null) : null
            }, query)
          }
        }
      }
    }
  })
}


