'use strict'

const joi = require('joi')

const base = {
	title: joi.string().required(),
	subtitle: joi.string().required(),
	imageUrl: joi.string().required(),
	url: joi.string().required(),
	idCity: joi.number().allow('', null)
}

const create = {
	...base
}

const update = {
	...base
}

module.exports = {
	validations: {
		create: joi.object().keys(create),
		update: joi.object().keys(update)
	},
	name: 'news'
}
