'use strict'

const joi = require('joi')

const base = {
  nearQuarantine: joi.boolean().required(),
  position: joi.object()
    .keys({
      type: joi.string()
        .required()
        .valid(["Point"]),
      coordinates: joi.array().ordered([
        joi.number()
          .min(-90)
          .max(90)
          .required(),
        joi.number()
          .min(-180)
          .max(180)
          .required()
      ])
    }),
  idUser: joi.number()

}

const create = {
  ...base
}

const update = {
  ...base
}

const nearBy = {
  topLeft: base.position,
  topRight: base.position,
  bottomLeft: base.position,
  bottomRight: base.position

}

const adminNearBy = {
  topLeft: base.position,
  topRight: base.position,
  bottomLeft: base.position,
  bottomRight: base.position,
  status: joi.string(),
}

const newPoint = {
  point: base.position,
}
module.exports = {
  validations: {
    create: joi.object().keys(create),
    update: joi.object().keys(update),
    nearBy: joi.object().keys(nearBy),
    newPoint: joi.object().keys(newPoint),
    adminNearBy: joi.object().keys(adminNearBy)
  },
  name: 'position'
}