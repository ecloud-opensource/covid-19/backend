'use strict'

const joi = require('joi')

const base = {
	firstName: joi.string(),
	lastName: joi.string(),
	email: joi.string(),
	identification: joi.string(),
	phone: joi.string(),
	address: joi.string(),
	number: joi.string(),
	isQuarantined: joi.boolean(),
	finishQuarantined: joi.boolean(),
	quarantineFrom: joi.date(),
	quarantineTo: joi.date(),
	gender: joi.string(),
	avatarUrl: joi.string(),
	pregnant: joi.boolean(),
	onMovement: joi.boolean(),
	outOfSafehouse: joi.boolean(),
	probability: joi.number(),
	status: joi.string(),
	idCountry: joi.number(),
	idNeighborhood: joi.number(),
	idCity: joi.number(),
	idZone: joi.number(),
	idLatestPosition: joi.number(),
	isDeleted: joi.boolean(),
	safehouse: joi.object()
		.keys({
			type: joi.string()
				.required()
				.valid(["Point"]),
			coordinates: joi.array().ordered([
				joi.number()
					.min(-180)
					.max(180)
					.required(),
				joi.number()
					.min(-90)
					.max(90)
					.required()
			])
		}),
}

const create = {
	...base,
	phone: joi.string().required(),
	identification: joi.string().required(),
}

const update = {
	...base,
}

const contact = {
	firstName: joi.string().required(),
	lastName: joi.string().required(),
	email: joi.string().required(),
	message: joi.string().required()
}

module.exports = {
	validations: {
		create: joi.object().keys(create),
		update: joi.object().keys(update),
		contact: joi.object().keys(contact),
	},
	name: 'user'
}

