'use strict'

const joi = require('joi')

const create = {
	name: joi.string().required(),
	center: joi.object().required(),
	area: joi.object().required(),
	code: joi.string().required()
}

const update = {
	name: joi.string(),
	center: joi.object(),
	area: joi.object(),
	code: joi.string(),
	areInfected: joi.number()
}

module.exports = {
	validations: {
		create: joi.object().keys(create),
		update: joi.object().keys(update)
	},
	name: 'country'
}
