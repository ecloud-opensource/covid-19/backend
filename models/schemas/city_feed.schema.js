'use strict'

const joi = require('joi')

const base = {
	url: joi.string().required(),
	title: joi.string().required(),
	image: joi.string(),
	description: joi.string().required(),
	order: joi.number().required(),
	idCity: joi.number().required(),
}

const create = {
	...base
}

const update = {
	...base
}

module.exports = {
	validations: {
		create: joi.object().keys(create),
		update: joi.object().keys(update)
	},
	name: 'cityFeed'
}