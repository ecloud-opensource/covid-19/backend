'use strict'

const joi = require('joi')

const base = {
  phone: joi.string().required(),
  validated: joi.boolean(),
  idUser: joi.number(),
  identification: joi.string(),
  code: joi.string()
}

const create = {
  ...base
}

const update = {
  ...base,
  firebaseToken: joi.string().required(),
  code: joi.string().required(),

}

module.exports = {
  validations: {
    create: joi.object().keys(create),
    update: joi.object().keys(update)
  },
  name: 'phoneValidation'
}