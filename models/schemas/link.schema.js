'use strict'

const joi = require('joi')

const base = {
	url: joi.string().required(),
	title: joi.string().required(),
	image: joi.string(),
	description: joi.string().required(),
	order: joi.number().required(),
	idCountry: joi.number(),
	idCity: joi.number(),
}

const create = {
	...base
}

const update = {
	...base
}

module.exports = {
	validations: {
		create: joi.object().keys(create),
		update: joi.object().keys(update)
	},
	name: 'link'
}