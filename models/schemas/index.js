/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
const Joi = require('joi')
const fs = require('fs')
const path = require('path')

const basename = path.basename(module.filename)

const schemas = {}

// Loader
fs.readdirSync(__dirname)
	.filter(file => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
	.forEach(file => {
		const schema = require(path.join(__dirname, file))
		schemas[schema.name] = schema.validations
	})

const validator = (schema) => async (req, res, next) => {
	const { body } = req

	Joi.validate(body, schema, err => {
		if (err) {
			return next(new Error(err.details[0].message))
		}

		return next()
	})
}

module.exports = {
	validator,
	...schemas
}
