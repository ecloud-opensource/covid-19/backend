'use strict'

const joi = require('joi')

const getOne = {
	id: joi.string().required()
}

module.exports = {
	validations: {
		getOne: joi.object().keys(getOne)
	},
	name: 'base'
}
