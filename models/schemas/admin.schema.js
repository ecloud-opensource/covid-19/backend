'use strict'

const joi = require('joi')

const base = {
	firstName: joi.string().required(),
	lastName: joi.string().required(),
	password: joi.string(),
	avatarUrl: joi.string().allow('', null),
	email: joi.string().required(),
	identification: joi.string().required(),
	profession: joi.string().allow('', null),
	workplace: joi.string().allow('', null),
	phone: joi.string().allow('', null),
	profile: joi.string().valid('operator', 'super_admin').required(),
	cities: joi.array()
}

const create = {
	...base,
	password: joi.string().required()
}

const update = {
	...base
}

module.exports = {
	validations: {
		create: joi.object().keys(create),
		update: joi.object().keys(update)
	},
	name: 'admin'
}
