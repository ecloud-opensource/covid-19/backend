'use strict'

const joi = require('joi')

const base = {
	name: joi.string().required(),
	center: joi.object().required(),
	area: joi.object().required(),
	idNeighborhood: joi.number().required(),
	code: joi.string().required()
}

const create = {
	...base
}

const update = {
	...base
}

module.exports = {
	validations: {
		create: joi.object().keys(create),
		update: joi.object().keys(update)
	},
	name: 'zone'
}
