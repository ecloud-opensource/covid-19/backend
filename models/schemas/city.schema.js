'use strict'

const joi = require('joi')

const create = {
	name: joi.string().required(),
	center: joi.object().required(),
	area: joi.object().required(),
	idCountry: joi.number().required(),
	code: joi.string().required(),
	areInfected: joi.number()
}

const update = {
	name: joi.string(),
	center: joi.object(),
	area: joi.object(),
	idCountry: joi.number(),
	code: joi.string(),
	areInfected: joi.number()
}

module.exports = {
	validations: {
		create: joi.object().keys(create),
		update: joi.object().keys(update)
	},
	name: 'city'
}
