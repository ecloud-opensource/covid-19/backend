'use strict'

const baseModel = require('models/base_model/base.model')
const logHook = require('models/base_model/logHook')

/**
 * @apiDefine CityModel
 * @apiParam {String} name Name
 * @apiParam {String} code Code
 * @apiParam {Number} areInfected Are infected
 * @apiParam {Number} areQuarantined Are Quarantined
 * @apiParam {Number} areOnRisk Are On Risk
 * @apiParam {Number} areExposed Are Exposed
 * @apiParam {Number} areWithSymptoms Are With Symptoms
 * @apiParam {Number} areHealthy Are Healthy
 * @apiParam {Number} fellows Fellows
 * @apiParam {[point](#api-Modelos-ObjectPoint)} center Center
 * @apiParam {[polygon](#api-Modelos-ObjectPolygon)} area Area
 */

/**
 * @apiDefine CityRelations
 * @apiParam {[neighborhood[]](#api-Modelos-ObjectNeighborhood)} neighborhoods Neighborhoods
 * @apiParam {[country](#api-Modelos-ObjectCountry)} country Country
 * @apiParam {[state](#api-Modelos-ObjectState)} state State
 */

/**
 * @api {OBJECT} city City
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse CityModel
 * @apiUse CityRelations
 * 
 * 
 * @apiDescription Available Scopes
 * - country: Country
 * - neighborhoods: Neighborhoods
 */
module.exports = (sequelize, DataTypes) => {
	const entity = sequelize.define('city', {

		...baseModel,

		name: {
			type: DataTypes.STRING
		},

		code: {
			type: DataTypes.STRING
		},

		areInfected: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_infected'
		},

		areQuarantined: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_quarantined'
		},

		areOnRisk: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_on_risk'
		},

		areExposed: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_exposed'
		},

		areWithSymptoms: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_with_symptoms'
		},

		areHealthy: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
			field: 'are_healthy'
		},


		fellows: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		},

		center: {
			type: DataTypes.GEOMETRY('POINT')
		},

		area: {
			type: DataTypes.GEOMETRY('POLYGON')
		},

		termsAndConditions: {
			type: DataTypes.STRING,
			field: 'terms_and_conditions'
		},

	}, {
		tableName: 'city'
	})

	entity.associate = (models) => {
		entity.belongsTo(models.state, { foreignKey: { field: 'id_state', name: 'idState' }, as: 'state' })
		entity.belongsTo(models.country, { foreignKey: { field: 'id_country', name: 'idCountry' }, as: 'country' })
		entity.hasMany(models.neighborhood, { foreignKey: { field: 'id_city', name: 'idCity' }, as: 'neighborhoods' })
		entity.hasMany(models.link, { foreignKey: { field: 'id_city', name: 'idCity' }, as: 'link' })
		entity.hasMany(models.cityFeed, { foreignKey: { field: 'id_city', name: 'idCity' } })
		entity.hasMany(models.cityLog, { foreignKey: { field: 'id_city', name: 'idCity' } })
	}

	entity.loadScopes = (models) => {
		entity.addScope('country', {
			include: [
				{
					model: models.country,
					required: true,
					as: 'country'
				}
			]
		})

		entity.addScope('state', {
			include: [
				{
					model: models.state,
					required: true,
					as: 'state'
				}
			]
		})

		entity.addScope('neighborhoods', {
			include: [
				{
					model: models.neighborhood,
					required: false,
					as: 'neighborhoods'
				}
			]
		})
	}

	entity.loadHooks = (models) => {
		logHook(entity, models.cityLog, "idCity")
	}

	return entity
}
