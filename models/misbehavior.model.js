'use strict'

const baseModel = require('models/base_model/base.model')
const moment = require("moment")
/**
 * @apiDefine MisbehaviorModel
 * @apiParam {Date} from Misbehavior from
 * @apiParam {Date} to Misbehavior to
 *
 */

/**
 * @apiDefine MisbehaviorRelations
 * @apiParam {[user](#api-Modelos-ObjectUser)} user User
 * @apiParam {[city](#api-Modelos-ObjectCity)} city City
 * @apiParam {[country](#api-Modelos-ObjectCountry)} country Country
 * @apiParam {[neighborhood](#api-Modelos-ObjectNeighborhood)} neighborhood Neighborhood
 */

/**
 * @api {OBJECT} misbehavior Misbehavior
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse MisbehaviorModel
 * @apiUse MisbehaviorRelations
 * 
 * @apiDescription Available Scopes
 * - user: user
 */
module.exports = (sequelize, DataTypes) => {
  const entity = sequelize.define('misbehavior', {

    ...baseModel,

    from: {
      type: DataTypes.DATE,
      defaultValue: moment()
    },

    to: {
      type: DataTypes.DATE,
    },

  }, {
    tableName: 'misbehavior'
  })

  entity.associate = (models) => {
    entity.belongsTo(models.user, { foreignKey: { field: 'id_user', name: 'idUser' } })
    entity.belongsTo(models.city, { foreignKey: { field: 'id_city', name: 'idCity' } })
    entity.belongsTo(models.country, { foreignKey: { field: 'id_country', name: 'idCountry' } })
    entity.belongsTo(models.neighborhood, { foreignKey: { field: 'id_neighborhood', name: 'idNeighborhood' } })
  }

  entity.loadScopes = (models) => {
    entity.addScope('user', {
      include: [
        {
          model: models.user,
          required: true,
          as: 'user'
        }
      ]
    })
  }
  return entity

}
