'use strict'

const baseModel = require('models/base_model/base.model')

/**
 * @apiDefine CityFeedModel
 * @apiParam {String} url URL
 * @apiParam {String} title Title
 * @apiParam {String} image imageUrl
 * @apiParam {String} description Description
 * @apiParam {Number} order Order
 */

/**
 * @apiDefine CityFeedRelations
 * @apiParam {[city](#api-Modelos-ObjectCity)} city City
 */

/**
 * @api {OBJECT} cityFeed City Feed
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse CityFeedModel
 * @apiUse CityFeedRelations
 * 
 * @apiDescription Available Scopes
 * - city: City
 */
module.exports = (sequelize, DataTypes) => {
	const entity = sequelize.define('cityFeed', {

		...baseModel,

		url: {
			type: DataTypes.STRING
		},

		title: {
			type: DataTypes.STRING
		},

		image: {
			type: DataTypes.STRING
		},

		description: {
			type: DataTypes.TEXT
		},

		order: {
			type: DataTypes.INTEGER
		}


	}, {
		tableName: 'city_feed'
	})

	entity.associate = (models) => {
		entity.belongsTo(models.city, { foreignKey: { field: 'id_city', name: 'idCity' }, as: 'city' })
	}

	entity.loadScopes = (models) => {
		entity.addScope('city', {
			include: [
				{
					model: models.city,
					required: true,
					as: 'city'
				}
			]
		})
	}

	return entity
}
