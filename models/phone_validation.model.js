'use strict'

const baseModel = require('models/base_model/base.model')

/**
 * @apiDefine PhoneValidationModel
 * @apiParam {String} phone Phone
 * @apiParam {String} code Code
 * @apiParam {Boolean} validated Is Validated
 */

/**
* @apiDefine PhoneValidationRelations
* @apiParam {[user](#api-Modelos-ObjectUser)} user User
*/

/**
 * @api {OBJECT} phoneValidation Phone Validation
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse PhoneValidationModel
 * @apiUse PhoneValidationRelations
 */
module.exports = (sequelize, DataTypes) => {
	const entity = sequelize.define('phoneValidation', {

		...baseModel,

		phone: {
			type: DataTypes.STRING
		},

		code: {
			type: DataTypes.STRING
		},

		validated: {
			type: DataTypes.BOOLEAN,
			defaultValue: false
		}


	}, {
		tableName: 'phone_validation',
		indexes: [
			{ unique: true, fields: ['phone', 'is_deleted'], name: "indx_validation_phone" },
		]
	})


	entity.associate = (models) => {
		entity.belongsTo(models.user, { foreignKey: { field: 'id_user', name: 'idUser' } })
	}


	return entity
}

