'use strict'

const baseModel = require('models/base_model/base.model')
const { distanceInMetersBeetwenTwoCoords } = require("../helpers/geo.helper")
/**
 * @apiDefine PositionModel
 * @apiParam {[point](#api-Modelos-ObjectPoint)} position Position
 * @apiParam {boolean} nearQuarantine Close up of a quarantined person
 * @apiParam {boolean} outOfSafehouse If you leave your safehouse
 * @apiParam {boolean} onMovement If you are on movement
 *
 *
 */

/**
 * @apiDefine PositionRelations
 * @apiParam {[user](#api-Modelos-ObjectUser)} user User
 *
 */



/**
 * @api {OBJECT} Position Position
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse PositionModel
 * @apiUse PositionRelations
 * 
 * @apiDescription Available Scopes
 * - positionsWithMatch: position with match
 */

/**
 * @apiDefine PointModel
 * @apiParam {string="Point"} type Geometric type
 * @apiParam {Double[]} coordinates Coordinates
 * 
 */

/**
 * @apiDefine PointRelations
 */

/**
 * @api {OBJECT} Point Point
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse PointModel
 * @apiUse PointRelations
 */

/**
 * @apiDefine PolygonModel
 * @apiParam {string="Polygon"} type Geometric type
 * @apiParam {Integer[][][]} coordinates Coordinates
 */

/**
 * @apiDefine PolygonRelations
 */

/**
 * @api {OBJECT} Polygon Polygon
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse PolygonModel
 * @apiUse PolygonRelations
 */

module.exports = (sequelize, DataTypes) => {
  const entity = sequelize.define('position', {

    ...baseModel,

    position: {
      type: DataTypes.GEOMETRY('POINT')
    },

    nearQuarantine: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },

    outOfSafehouse: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },

    onMovement: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }

  }, {
    tableName: 'position'
  })


  entity.associate = (models) => {
    entity.belongsTo(models.user, { foreignKey: { field: 'id_user', name: 'idUser' }, constraints: false, as: 'user' })
    entity.hasMany(models.positionMatch, { foreignKey: { field: 'id_postion', name: 'idPosition' }, constraints: false, as: 'match' })
  }
  entity.loadScopes = (models) => {

    entity.addScope('positionsWithMatch', {
      include: [
        {
          model: models.positionMatch,
          required: false,
          as: 'match',
          include: [
            {
              model: models.user,
              required: true,
              as: 'user'
            }
          ]
        }
      ]
    })
  }

  entity.loadHooks = (models) => {
    entity.addHook('afterCreate', async (instance, options) => {
      let user = options.user

      let userToUpdate = {};

      userToUpdate.idLatestPosition = instance.id;

      if (user && user.idLatestPosition) {


        const [position, safehouseRange] = await Promise.all([
          models.position.findById(user.idLatestPosition),
          models.setting.findOne({ where: { code: 'SAFEHOUSE_RANGE_AREA' } })
        ])

        const distanceStill = distanceInMetersBeetwenTwoCoords(
          position.position.coordinates[0],
          position.position.coordinates[1],
          instance.position.coordinates[0],
          instance.position.coordinates[1]
        );

        userToUpdate.onMovement = distanceStill > __config.maxDistanceOfAPersonStill ? true : false;

        instance.set('onMovement', userToUpdate.onMovement)

        const distanceSafeHouse = distanceInMetersBeetwenTwoCoords(
          user.safehouse.coordinates[0],
          user.safehouse.coordinates[1],
          instance.position.coordinates[0],
          instance.position.coordinates[1]
        );

        userToUpdate.outOfSafehouse = distanceSafeHouse > +safehouseRange.value ? true : false;

        instance.set('outOfSafehouse', userToUpdate.outOfSafehouse);


      }


      models.user.update(userToUpdate, { where: { id: instance.idUser } })

      instance.save();

    })


  }

  return entity
}

