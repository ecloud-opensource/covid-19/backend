'use strict'

const baseModel = require('models/base_model/base.model')


/**
 * @apiDefine PositionMatchModel
 * @apiParam {string} status Status of match person
 * @apiParam {string} onMovement Movement of match person
 * @apiParam {string} isQuarantined Qurantined of match person
 * @apiParam {string} outOfSafehouse Safehouse status of match person
 * @apiParam {number} distance Distance of match person
 *
 */

/**
 * @apiDefine PositionMatchRelations
 * @apiParam {[user](#api-Modelos-ObjectUser)} user User
 * @apiParam {[position](#api-Modelos-ObjectPosition)} position Position
 *
 */

/**
 * @api {OBJECT} PositionMatch PositionMatch
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse PositionMatchModel
 * @apiUse PositionMatchRelations
 */

module.exports = (sequelize, DataTypes) => {
  const entity = sequelize.define('positionMatch', {

    ...baseModel,

    status: {
      type: DataTypes.STRING
    },

    outOfSafehouse: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },

    onMovement: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },

    isQuarantined: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },

    distance: {
      type: DataTypes.FLOAT,
      defaultValue: 0
    }


  }, {
    tableName: 'position_match'
  })


  entity.associate = (models) => {
    entity.belongsTo(models.user, { foreignKey: { field: 'id_user', name: 'idUser' }, constraints: false })
    entity.belongsTo(models.position, { foreignKey: { field: 'id_position', name: 'idPosition' }, constraints: false })
  }

  entity.loadHooks = (models) => {
  }

  return entity
}

