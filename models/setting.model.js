'use strict'

const baseModel = require('models/base_model/base.model')

/**
 * @apiDefine SettingModel
 * @apiParam {String} name Name
 * @apiParam {String} code Code
 * @apiParam {String} description Description
 * @apiParam {String} value Value
 *
 */

/**
 * @apiDefine SettingRelations
 */

/**
 * @api {OBJECT} setting Setting
 * @apiGroup Modelos
 * @apiVersion 1.0.0
 * @apiParam {Integer} id ID.
 * @apiUse SettingModel
 * @apiUse SettingRelations
 */
module.exports = (sequelize, DataTypes) => {
  const entity = sequelize.define('setting', {

    ...baseModel,

    name: {
      type: DataTypes.STRING
    },

    code: {
      type: DataTypes.STRING
    },

    description: {
      type: DataTypes.TEXT
    },

    value: {
      type: DataTypes.STRING
    },

    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },


  }, {
    tableName: 'setting'
  })
  return entity
}
