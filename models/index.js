'use strict'

const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')

const basename = path.basename(module.filename)

const db = {}

const sequelize = new Sequelize(__config.sequelize.database,
	__config.sequelize.username,
	__config.sequelize.password,
	{
		dialect: __config.sequelize.dialect,
		host: __config.sequelize.host,
		define: {
			timestamps: true,
			freezeTableName: true,
			underscored: true,
			createdAt: 'createdAt',
			updatedAt: 'updatedAt'
		},
		omitNull: false,
		logging: false
	})

sequelize.sync({ alter: true })

fs.readdirSync(__dirname)
	.filter(file => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
	.forEach(file => {
		const model = sequelize.import(path.join(__dirname, file))
		db[model.name] = model
	})


// Load models
Object.keys(db).forEach(modelName => {
	if (db[modelName].associate) {
		db[modelName].associate(db)
	}
})


// Load scopes & hooks
Object.keys(db).forEach(modelName => {
	db[modelName].addScope('defaultScope', {
		where: {
			isDeleted: false
		},
		order: [['id', 'DESC']]
	}, { override: true })

	db[modelName].addScope("allFields", {
		where: {
			isDeleted: false,
		}
	}, { override: true });

	if (db[modelName].loadScopes) {
		db[modelName].loadScopes(db)
	}
	if (db[modelName].loadHooks) {
		db[modelName].loadHooks(db)
	}
})

sequelize.authenticate()
	.then(() => {
		__logger.info('Connected to DB.')
	})
	.catch(err => {
		__logger.error('Unable to connect to DB.')
		__logger.error(err)
	})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
