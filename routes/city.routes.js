const { Router } = require('express')

const {
	getAll,
	getOneById,
	getFeed,
	getLinks,
	getLog,
	create,
	update,
	deleteEntity,
	ranking,
	getAllMobile,
	getOneByIdMobile,
	adminRanking
} = require('controllers/city.controller')

const asyncMiddleware = require('middleware/async.middleware')
const { validateAccess } = require('middleware/authentication.middleware')

const router = Router()

const schemas = require('models/schemas')

router.get('/', validateAccess(["admin"]), asyncMiddleware(getAll))
router.get('/mobile', validateAccess(["user"]), asyncMiddleware(getAllMobile))
router.get('/feed', validateAccess(["user"]), asyncMiddleware(getFeed))
router.get('/admin-ranking', validateAccess(["admin"]), asyncMiddleware(adminRanking))
router.get('/ranking', validateAccess(["user"]), asyncMiddleware(ranking))
router.get('/links', validateAccess(["user"]), asyncMiddleware(getLinks))
router.get('/log', validateAccess(["admin"]), asyncMiddleware(getLog))
router.get('/:id', validateAccess(["admin"]), asyncMiddleware(getOneById))
router.get('/mobile/:id', validateAccess(["user"]), asyncMiddleware(getOneByIdMobile))
router.post('/', validateAccess(["admin"]), schemas.validator(schemas.city.create), asyncMiddleware(create))
router.put('/:id', validateAccess(["admin"]), schemas.validator(schemas.city.update), asyncMiddleware(update))
router.delete('/:id', validateAccess(["admin"]), asyncMiddleware(deleteEntity))

module.exports = router
