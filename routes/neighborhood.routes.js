const { Router } = require('express')

const {
	getAll,
	getOneById,
	getLog,
	create,
	update,
	deleteEntity,
	ranking,
	adminRanking,
	getAllMobile,
	getOneByIdMobile
} = require('controllers/neighborhood.controller')

const asyncMiddleware = require('middleware/async.middleware')
const { validateAccess } = require('middleware/authentication.middleware')

const router = Router()

const schemas = require('models/schemas')

router.get('/', validateAccess(["admin"]), asyncMiddleware(getAll))
router.get('/mobile', validateAccess(["user"]), asyncMiddleware(getAllMobile))
router.get('/ranking', validateAccess(["user"]), asyncMiddleware(ranking))
router.get('/admin-ranking', validateAccess(["admin"]), asyncMiddleware(adminRanking))
router.get('/log', validateAccess(["admin"]), asyncMiddleware(getLog))
router.get('/:id', validateAccess(["admin"]), asyncMiddleware(getOneById))
router.get('/mobile/:id', validateAccess(["user"]), asyncMiddleware(getOneByIdMobile))
router.post('/', validateAccess(["admin"]), schemas.validator(schemas.neighborhood.create), asyncMiddleware(create))
router.put('/:id', validateAccess(["admin"]), schemas.validator(schemas.neighborhood.update), asyncMiddleware(update))
router.delete('/:id', validateAccess(["admin"]), asyncMiddleware(deleteEntity))

module.exports = router
