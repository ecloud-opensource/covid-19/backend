const { Router } = require('express')

const {
	login,
	forgotPassword,
	getAll,
	resetPassword,
	getOneById,
	create,
	update,
	deleteEntity
} = require('controllers/admin.controller')

const asyncMiddleware = require('middleware/async.middleware')
const { validateAccess } = require('middleware/authentication.middleware')

const router = Router()

const schemas = require('models/schemas')

router.post('/login', asyncMiddleware(login))
router.put('/forgot-password', asyncMiddleware(forgotPassword))
router.put('/reset-password', asyncMiddleware(resetPassword))

router.get('/', validateAccess(["admin"]), asyncMiddleware(getAll))
router.get('/:id', validateAccess(["admin"]), asyncMiddleware(getOneById))
router.post('/', validateAccess(["admin"]), schemas.validator(schemas.admin.create), asyncMiddleware(create))
router.put('/:id', validateAccess(["admin"]), schemas.validator(schemas.admin.update), asyncMiddleware(update))
router.delete('/:id', validateAccess(["admin"]), asyncMiddleware(deleteEntity))

module.exports = router
