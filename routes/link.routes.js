const { Router } = require('express')

const {
	getAll,
	getOneById,
	create,
	update,
	deleteEntity
} = require('controllers/link.controller')

const asyncMiddleware = require('middleware/async.middleware')
const { validateAccess } = require('middleware/authentication.middleware')

const router = Router()

const schemas = require('models/schemas')

router.get('/', validateAccess(["admin"]), asyncMiddleware(getAll))
router.get('/:id', validateAccess(["admin"]), asyncMiddleware(getOneById))
router.post('/', validateAccess(["admin"]), schemas.validator(schemas.link.create), asyncMiddleware(create))
router.put('/:id', validateAccess(["admin"]), schemas.validator(schemas.link.update), asyncMiddleware(update))
router.delete('/:id', validateAccess(["admin"]), asyncMiddleware(deleteEntity))

module.exports = router
