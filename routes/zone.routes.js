const { Router } = require('express')

const {
	getAll,
	getOneById,
	create,
	update,
	deleteEntity
} = require('controllers/zone.controller')

const asyncMiddleware = require('middleware/async.middleware')
const { validateAccess } = require('middleware/authentication.middleware')

const router = Router()

const schemas = require('models/schemas')

router.get('/', validateAccess(["user"]), asyncMiddleware(getAll))
router.get('/:id', validateAccess(["user"]), asyncMiddleware(getOneById))
router.post('/', validateAccess(["user"]), schemas.validator(schemas.zone.create), asyncMiddleware(create))
router.put('/:id', validateAccess(["user"]), schemas.validator(schemas.zone.update), asyncMiddleware(update))
router.delete('/:id', validateAccess(["user"]), asyncMiddleware(deleteEntity))

module.exports = router
