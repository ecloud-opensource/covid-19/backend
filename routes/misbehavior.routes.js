const { Router } = require('express')

const {
  getAll,
} = require('controllers/misbehavior.controller')

const asyncMiddleware = require('middleware/async.middleware')
const { validateAccess } = require('middleware/authentication.middleware')

const router = Router()

const schemas = require('models/schemas')

router.get('/', validateAccess(["admin"]), asyncMiddleware(getAll))

module.exports = router
