const { Router } = require('express')

const asyncMiddleware = require('middleware/async.middleware')
const { getAll, getOneById, termsAndConditions, update } = require('controllers/setting.controller')
const { validateAccess } = require('middleware/authentication.middleware')

const router = Router()

router.get('/terms-and-conditions', validateAccess(["user", "any"]), asyncMiddleware(termsAndConditions))
router.get('/:code', asyncMiddleware(getOneById))
router.put('/:code', asyncMiddleware(update))

router.get('/', asyncMiddleware(getAll))

module.exports = router
