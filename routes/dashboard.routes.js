const { Router } = require('express')
const asyncMiddleware = require('middleware/async.middleware')
const { validateAccess } = require('middleware/authentication.middleware')
const { mainDashboard, dashboardGraph } = require("../controllers/dashboard.controller")

const router = Router()

router.get("/main", validateAccess(["admin"]), asyncMiddleware(mainDashboard))
router.get("/graph", validateAccess(["admin"]), asyncMiddleware(dashboardGraph))

module.exports = router