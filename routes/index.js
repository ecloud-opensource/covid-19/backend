const { input } = require('middleware/request-logger.middleware')
const interceptor = require('middleware/request-interceptor.middleware')
const { error, notFound } = require('helpers/response.helper')
const express = require('express')
const adminRoutes = require('routes/admin.routes')
const cityRoutes = require('routes/city.routes')
const cityFeedRoutes = require('routes/city_feed.routes')
const countryRoutes = require('routes/country.routes')
const dashboardRoutes = require('routes/dashboard.routes')
const neighborhoodRoutes = require('routes/neighborhood.routes')
const stateRoutes = require('routes/state.routes')
const uploadRoutes = require('routes/upload.routes')
const userRoutes = require('routes/user.routes')
const zoneRoutes = require('routes/zone.routes')
const geoRoutes = require('routes/geo.routes')
const settingRoutes = require('routes/setting.routes')
const linkRoutes = require('routes/link.routes')
const misbehaviorRoutes = require('routes/misbehavior.routes')

module.exports = (app) => {
	app.use(input)

	// Intercept request for query builder
	app.use(interceptor)

	// Docs
	app.use('/doc', express.static(`${process.env.PWD}/api-docs`))

	app.use('/admins', adminRoutes)
	app.use('/city', cityRoutes)
	app.use('/city-feed', cityFeedRoutes)
	app.use('/country', countryRoutes)
	app.use('/dashboard', dashboardRoutes)
	app.use('/neighborhood', neighborhoodRoutes)
	app.use('/state', stateRoutes)
	app.use('/upload', uploadRoutes)
	app.use('/zone', zoneRoutes)
	app.use('/user', userRoutes)
	app.use('/geo', geoRoutes)
	app.use('/setting', settingRoutes)
	app.use('/links', linkRoutes)
	app.use('/misbehavior', misbehaviorRoutes)

	app.use(notFound)
	app.use(error)
}
