const { Router } = require('express')

const {
	phoneValidation,
	codeValidation,
	contact,
	updateMe,
	updateById,
	getAll,
	getById,
	getMe,
	doQuestionnaire,
	getQuestionnaire,
	getQuestionnaireForAdmin,
	deleteEntity,
	getPositions
} = require('controllers/user.controller')

const asyncMiddleware = require('middleware/async.middleware')

const { validateAccess } = require('middleware/authentication.middleware')

const router = Router()

const schemas = require('models/schemas')

router.get('/', validateAccess(["admin"]), asyncMiddleware(getAll))
router.get('/me', validateAccess(["user"]), asyncMiddleware(getMe))
router.get('/position', validateAccess(["admin"]), asyncMiddleware(getPositions))
router.get('/questionnaire', validateAccess(["user"]), asyncMiddleware(getQuestionnaire))
router.get('/admin-questionnaire/:idUser', validateAccess(["admin"]), asyncMiddleware(getQuestionnaireForAdmin))
router.get('/:id', validateAccess(["admin"]), asyncMiddleware(getById))
router.post('/phone-validation', schemas.validator(schemas.phoneValidation.create), asyncMiddleware(phoneValidation))
router.post('/code-validation', schemas.validator(schemas.phoneValidation.update), asyncMiddleware(codeValidation))
router.post('/contact', validateAccess(["user"]), schemas.validator(schemas.user.contact), asyncMiddleware(contact))
router.put('/questionnaire', validateAccess(["user"]), asyncMiddleware(doQuestionnaire))
router.put('/me', validateAccess(["user"]), schemas.validator(schemas.user.update), asyncMiddleware(updateMe))
router.put('/:id', validateAccess(["admin"]), schemas.validator(schemas.user.update), asyncMiddleware(updateById))
router.delete('/:id', validateAccess(["admin"]), asyncMiddleware(deleteEntity))

module.exports = router