const { Router } = require('express')
const multer = require('multer')

const asyncMiddleware = require('middleware/async.middleware')
const { validateAccess } = require('middleware/authentication.middleware')
const { setFileName, validateUploadImage } = require('middleware/multer.middleware')
const { uploadImage } = require('controllers/upload.controller')

const storage = multer.diskStorage({
	destination: `${__dirname}/../../uploads/img`,
	filename: setFileName
})

const upload = multer({
	storage,
	fileFilter: validateUploadImage
})

const router = Router()

router.post('/image', validateAccess(["admin", "user"]), upload.single('file'), asyncMiddleware(uploadImage))

module.exports = router
