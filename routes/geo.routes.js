const { Router } = require('express')

const {
  userNearBy,
  newPoint,
  adminNearBy
} = require('controllers/geo.controller')

const asyncMiddleware = require('middleware/async.middleware')

const { validateAccess } = require('middleware/authentication.middleware')

const router = Router()

const schemas = require('models/schemas')

router.post('/nearby', validateAccess(["user"]), schemas.validator(schemas.position.nearBy), asyncMiddleware(userNearBy))
router.post('/admin-nearby', validateAccess(["admin"]), schemas.validator(schemas.position.adminNearBy), asyncMiddleware(adminNearBy))
router.post('/new-point', validateAccess(["user"]), schemas.validator(schemas.position.newPoint), asyncMiddleware(newPoint))

module.exports = router