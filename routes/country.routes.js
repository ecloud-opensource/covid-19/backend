const { Router } = require('express')

const {
	getAll,
	getOneById,
	create,
	update,
	deleteEntity,
	getAllMobile,
	getOneByIdMobile
} = require('controllers/country.controller')

const asyncMiddleware = require('middleware/async.middleware')
const { validateAccess } = require('middleware/authentication.middleware')

const router = Router()

const schemas = require('models/schemas')

router.get('/', validateAccess(["admin"]), asyncMiddleware(getAll))
router.get('/mobile', validateAccess(["user"]), asyncMiddleware(getAllMobile))
router.get('/:id', validateAccess(["admin"]), asyncMiddleware(getOneById))
router.get('/mobile/:id', validateAccess(["user"]), asyncMiddleware(getOneByIdMobile))
router.post('/', validateAccess(["admin"]), schemas.validator(schemas.country.create), asyncMiddleware(create))
router.put('/:id', validateAccess(["admin"]), schemas.validator(schemas.country.update), asyncMiddleware(update))
router.delete('/:id', validateAccess(["admin"]), asyncMiddleware(deleteEntity))

module.exports = router
