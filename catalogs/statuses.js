'use strict'

module.exports = {
  CODE: {
    QUARANTINED: 'QUARANTINED',
    ON_RISK: 'ON_RISK',
    EXPOSED: 'EXPOSED',
    WITH_SYMPTOMS: 'WITH_SYMPTOMS',
    HEALTHY: 'HEALTHY'
  },
  FIELD: {
    QUARANTINED: 'areQuarantined',
    ON_RISK: 'areOnRisk',
    EXPOSED: 'areExposed',
    WITH_SYMPTOMS: 'areWithSymptoms',
    HEALTHY: 'areHealthy'
  }

}
