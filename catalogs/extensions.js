'use strict'

module.exports = {
	CATALOG_REGEX: /\.(csv|xls|ssml|xlsb|xlsx|qpw|uos|eth|rtf|prn|sylk|dif|dbf|txt|html|fods|ods)$/,
	IMAGE_REGEX: /\.(jpg|jpeg|png)$/
}
